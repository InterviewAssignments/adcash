-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `adcash`;
CREATE DATABASE `adcash` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `adcash`;

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

TRUNCATE `customers`;
INSERT INTO `customers` (`customer_id`, `fullname`, `created_date`) VALUES
(1,	'John Doe',	'2019-07-14 18:42:35'),
(2,	'Laura Stone',	'2019-07-14 18:43:09'),
(3,	'Jon Olsson',	'2019-07-14 18:43:29'),
(4,	'Payam Naderi',	'2019-07-14 18:43:42');

DROP TABLE IF EXISTS `factors`;
CREATE TABLE `factors` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `price_promotions` decimal(6,2) DEFAULT NULL,
  `price_total` decimal(6,2) NOT NULL,
  `currency` varchar(4) NOT NULL,
  `items_count` int(11) NOT NULL,
  `quantities_total` int(11) NOT NULL,
  `status` varchar(12) NOT NULL DEFAULT 'unpaid',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  PRIMARY KEY (`order_id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `factors_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

TRUNCATE `factors`;
INSERT INTO `factors` (`order_id`, `customer_id`, `price_promotions`, `price_total`, `currency`, `items_count`, `quantities_total`, `status`, `created_date`) VALUES
(1,	1,	-0.96,	3.84,	'EUR',	1,	3,	'unpaid',	'2019-07-17 11:10:58'),
(12,	4,	NULL,	4.80,	'EUR',	1,	3,	'unpaid',	'2019-07-19 20:15:19'),
(13,	4,	NULL,	4.80,	'EUR',	1,	3,	'unpaid',	'2019-07-19 19:23:56');

DROP TABLE IF EXISTS `factors_products`;
CREATE TABLE `factors_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `price_total` decimal(6,2) NOT NULL,
  `currency` varchar(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `factors_products_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `factors` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

TRUNCATE `factors_products`;
INSERT INTO `factors_products` (`id`, `order_id`, `product_id`, `quantity`, `price`, `price_total`, `currency`) VALUES
(1,	1,	1,	3,	1.60,	4.80,	'EUR'),
(12,	13,	1,	3,	1.60,	4.80,	'EUR'),
(16,	12,	1,	3,	1.60,	4.80,	'EUR');

DROP TABLE IF EXISTS `factors_promotions`;
CREATE TABLE `factors_promotions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `promo_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT 1,
  `price` decimal(6,2) NOT NULL,
  `currency` varchar(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `promo_id` (`promo_id`),
  CONSTRAINT `factors_promotions_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `factors` (`order_id`),
  CONSTRAINT `factors_promotions_ibfk_2` FOREIGN KEY (`promo_id`) REFERENCES `promotions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

TRUNCATE `factors_promotions`;
INSERT INTO `factors_promotions` (`id`, `order_id`, `promo_id`, `quantity`, `price`, `currency`) VALUES
(1,	1,	1,	1,	-0.96,	'EUR');

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL,
  `description` text DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `price` decimal(6,2) NOT NULL,
  `currency` varchar(4) NOT NULL DEFAULT 'EUR',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `updated_date` datetime DEFAULT NULL,
  `is_available` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

TRUNCATE `products`;
INSERT INTO `products` (`product_id`, `title`, `description`, `thumb`, `price`, `currency`, `created_date`, `updated_date`, `is_available`) VALUES
(1,	'Pepsi Cola NRB 250ml',	'PEPSI- THE BOLD, REFRESHING, ROBUST COLA\r\nTYPE: BOTTLES, CANS AND CARTONS	SIZE: 20 FL OZ\r\nNUTRITION INFO:\r\nSERVING SIZE 8 FL OZ (240 ML)\r\nSERVINGS PER 20 FL OZ CONTAINER: 2.5	PER SERVING	PER CONTAINER\r\n8 FL OZ	%D',	'/statics/products/brand_1541414524.png',	1.60,	'EUR',	'2019-07-16 17:25:57',	NULL,	1);

DROP TABLE IF EXISTS `products_prop`;
CREATE TABLE `products_prop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `property` varchar(80) NOT NULL,
  `value` text NOT NULL,
  `group` varchar(80) DEFAULT NULL COMMENT 'when you want to group related items by name',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `products_prop_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

TRUNCATE `products_prop`;
INSERT INTO `products_prop` (`id`, `product_id`, `property`, `value`, `group`) VALUES
(1,	1,	'size',	'20 FL OZ',	NULL),
(2,	1,	'type',	'BOTTLES, CANS AND CARTONS',	NULL),
(3,	1,	'serving_size',	'240 ML',	'nutrition_info'),
(4,	1,	'sugar',	'8ml/100mg',	'nutrition_info');

DROP TABLE IF EXISTS `promotions`;
CREATE TABLE `promotions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

TRUNCATE `promotions`;
INSERT INTO `promotions` (`id`, `title`, `description`, `type`, `is_active`) VALUES
(1,	'Pepsi Promotion',	'Discount of 20% will be applied to the total cost of order when at least 3 items of Pepsi Cola are selected.\r\n',	'discount_pepsi',	1);

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(120) DEFAULT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

TRUNCATE `users`;
INSERT INTO `users` (`user_id`, `fullname`, `username`, `password`) VALUES
(1,	'Administrator',	'admin@mail.com',	'610a2ee688cda9e724885e23cd2cfdee');

-- 2019-07-19 20:31:50
