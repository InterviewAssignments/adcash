## How To Install

Clone Codes From Remote Git Repository. it's hosted under public gitlab repo.

```
git clone git@gitlab.com:InterviewAssignments/adcash.git && cd adcash

```

Set Workspace Environments:

copy env dist and if you want make change to environment variables.
!! default web service will use port 8000, you could change this by edit .env file

```
cd dckr && cp .env.dist .env
```

Run Docker:

Docker Compose:
- from ``` dckr ``` directory;
- ```docker-commpose up -d```

Make file:
- from ``` dckr ``` directory; you can see Makefile there
- type ```make up```


Note: By Default It's open 8000 port on your host to server http request, if it's not desirable change ``` .env ``` file.
[click here to view](http://localhost:8000)


## Credential:

Credential Needed To Access Application:

username: admin@mail.com
password: secretkey


## Try to import sample MariaDB data with command:
to access shell within mysql container

```
docker-compose exec mariadb /bin/bash
```

then import db data

```
mysql -u root -psecret adcash < sample_data.sql
```

##

if there was any problem don't hesitate to contact me: naderi.payam@gmail.com
