<?php
# override PT_DIR_ROOT of skeleton, looking for vendor!!
define('PT_DIR_ROOT', dirname(__DIR__));


if (! file_exists(PT_DIR_ROOT . '/vendor/autoload.php') )
    throw new \RuntimeException( "Unable to load Module.\n"
        . "- Type `composer install`; to install library dependencies.\n"
    );

require PT_DIR_ROOT . '/vendor/autoload.php';
require PT_DIR_ROOT . '/vendor/poirot/skeleton/index.php';
