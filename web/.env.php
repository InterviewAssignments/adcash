<?php
/** @see \Poirot\Std\Environment\EnvBase */
return [
     'defined_const' => [
        'PT_DIR_CONFIG' => constant('PT_DIR_ROOT').'/config/',
        'PT_DIR_DATA'   => constant('PT_DIR_ROOT').'/data/',
     ],
     'environments' => [
        #'YOUR_ENV'      => 'value',
     ],
];
