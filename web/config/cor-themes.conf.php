<?php
use Module\Themes\ThemeManager\ResolveStrategy\MergedConfigThemeResolver;

/*
 * Override default settings of Theme Module.
 *
 * basically to override default values you could copy config file from inside module
 * directory and change values here as project specific configs.
 *
 * note: *.local.conf.php files will excluded from git repository and will override *.conf.php files.
 */
return [
    MergedConfigThemeResolver::class => [
        'default_theme' => 'startbootstrap-sb-admin-2',
    ],
];
