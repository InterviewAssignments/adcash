<?php
return [
    'modules' => [
        // Enabled Modules
        'AdcashAsm'
    ],
    'module_manager' => [
        // Module Manager Specific Settings
        'modules_dir' => [
            PT_DIR_ROOT . '/Modules', # add module dir from project root
        ],
    ],
];
