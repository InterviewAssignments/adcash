(function($)
{
  function replaceUrlParam(paramName, paramValue, url){
    if (url === undefined)
        url = window.location.href;

    if (paramValue == null)
      paramValue = '';

    let pattern = new RegExp('\\b('+paramName+'=).*?(&|#|$)');
    if (url.search(pattern)>=0)
      return url.replace(pattern,'$1' + paramValue + '$2');

    url = url.replace(/[?#]$/,'');
    return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue;
  }

  $('.action-filter-date').click(function (e) {
    e.preventDefault();
    let value = $(this).data('browse');
    window.location.replace( replaceUrlParam('from', value) );
    return false;
  });

  $('select#search_area').on('change', function() {
    let data = getSearchAreaData();
    let disabled = undefined == data;

    if (disabled)
      $('select#search_term').val(null).trigger('change');

    $('select#search_term').prop('disabled', disabled);
  });

  $('select#search_term').on('change', function() {
    let data = getSearchTermData();
    let disabled = undefined == data;

    if (disabled)
      $('.action-search').addClass('disabled');
    else
      $('.action-search').removeClass('disabled');
  });

  $('.action-search').click(function () {
    let url = replaceUrlParam('scope', getSearchAreaData());
    url = replaceUrlParam('ident', getSearchTermData(), url);

    window.location.replace(url);
  });

  function getSearchAreaData() {
    return $("select#search_area option:selected").val();
  }

  function getSearchTermData() {
    return $("select#search_term option:selected").val();
  }

  let $lastClicked;
  $(document).on('click', '.btn-trash', function (e) {
    e.preventDefault();
    $lastClicked = $(this);
    return false;
  });

  $(document).on('click', '.action-order-delete', function (e) {
    let orderId = $lastClicked.data('id');
    let remoteApi = $lastClicked.data('href');
    console.log(remoteApi);

    $.ajax({
      url: remoteApi,
      headers: {
        Accept: "text/plain; charset=utf-8",
      },
      cache: false,
      success: function() {
        $('#order-row-' + orderId).fadeOut();
        $('#confirmDelete').modal('hide');
      }
    });
  });

})(jQuery); // End of use strict
