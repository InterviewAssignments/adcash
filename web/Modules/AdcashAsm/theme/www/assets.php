<?php
use Module\AssetManager\Resolvers\AggregateResolver;
use Module\AssetManager\Resolvers\DirectoryResolver;
use Module\AssetManager\Resolvers\GlobFileResolver;
use Module\AssetManager\Resolvers\PathPrefixResolver;

/*
 * All assets makes accessible to outside world from here.
 *
 * this file should return an instance of Resolver(iAssetResolver)
 * that is responsible to resolve assets.
 */

// CSS Resolver defined separately; later we can add FilterWrapper for example to minify
// or combine css files or both.
$globCssResolver = (new GlobFileResolver)
    ->setBaseDir(__DIR__) // assets are recognized with /css/file.css
    ->setGlobs([
        __DIR__ . '/css/*.css',
    ]);


// instead of setting base directory; also we can add path prefix wrapper
// do the same thing as above regarding to base path.
$globJsResolver = (new PathPrefixResolver(
    (new GlobFileResolver)
        ->setGlobs([
            __DIR__ . '/js/*.js',
        ])
))->setPath('/js'); // js files are recognized with /js/file.js


// other files in directory has no specific meaning to us from Filters perspective
// so just exclude js, css directories that we defined before and expose whole directory.
$dirResolver = (new DirectoryResolver())
    ->setDir(__DIR__)
    ->setExcludePaths(__DIR__.'/css', __DIR__.'/js');


// Put whole resolvers in AggregateResolver together as our final Resolver Object
$resolver = (new AggregateResolver)
    ->setResolvers($globCssResolver, $globJsResolver, $dirResolver);


return $resolver;
