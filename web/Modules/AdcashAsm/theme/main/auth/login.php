<?php
use Poirot\View\ViewModel\TwoStepViewCallable;

/*
 * Poirot Uses Two-Step View;
 *
 * you can make change to view variables values, change template for specific layout,
 * add widgets to place holders inside a page, etc...
 */

return new TwoStepViewCallable(function($parent, $self) {
    // change layout decorator template; login will uses simple layout
    // without top menu, footer, etc...
    $parent->root()
        ->setTemplate('layout.blank')
        ->setVariables([
            'bodyAttributes' => 'class="bg-gradient-primary"', // lets have a gradient on login page
        ])
    ;
});
