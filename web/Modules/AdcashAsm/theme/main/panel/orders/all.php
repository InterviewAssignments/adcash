<?php
use Poirot\View\ViewModel\TwoStepViewCallable;

/*
 * Poirot Uses Two-Step View;
 *
 * you can make change to view variables values, change template for specific layout,
 * add widgets to place holders inside a page, etc...
 */
$vars = get_defined_vars();
return new TwoStepViewCallable(function($parent, $self) use ($vars)
{
    $from = $vars['filters']['from'];
    if ($from == false)
        $from = 'all';
    elseif ($from == date('Y-m-d', time()))
        $from = 'today';
    elseif ($from == date('Y-m-d', strtotime('-7 days', time())))
        $from = '-7 days';

    $identTitle = 'Unknwon';
    if ($ident = $vars['filters']['ident']) {
        if ($vars['filters']['scope'] == 'customer') {
            $ident = \Module\AdcashAsm\Services\Repositories::CustomersRepo()
                ->findOneByUID($ident);
            $identTitle = $ident ? $ident->getFullname() : $identTitle;

        } elseif ($vars['filters']['scope'] == 'product') {
            $ident = \Module\AdcashAsm\Services\Repositories::ProductsRepo()
                ->findOneByUID($ident);

            $identTitle = $ident ? $ident->getTitle() : $identTitle;
        }
    }

    $parent->setVariables(array_replace_recursive($vars, [
        'filters' => [
            'from' => $from,
            'ident_title' => $identTitle,
        ],
    ]));
});
