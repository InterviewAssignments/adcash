<?php
use Module\AdcashAsm\Actions\Widgets;

return [
    'nested' => [
        'widgets' => [
            'services' => [
                Widgets::LatestOrders => Widgets\LatestOrdersWidget::class,
            ],
        ],
    ],
];
