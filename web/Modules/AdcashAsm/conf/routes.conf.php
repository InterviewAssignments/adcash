<?php

use Module\HttpRenderer\RenderStrategy\RenderRouterStrategy;
use Module\HttpFoundation\Events\Listener\ListenerDispatch;
use Module\HttpFoundation\Events\Listener\ListenerDispatchResult;
use Module\HttpFoundation\Response\ResponseRedirect;
use Poirot\Router\Route\RouteMethodSegment;
use Poirot\Router\Route\RouteSegment;


return
[
    ## Serve Static Product Files
    #
    'statics' => [
        'route' => RouteMethodSegment::class,
        'options' => [
            'method'   => 'GET',
            'criteria' => '/statics/:file~.+~',
            'match_whole' => false,
        ],
        'params' => [
            ListenerDispatch::ACTIONS => \Poirot\Ioc\newInitIns(new \Poirot\Ioc\instance(
                \Module\HttpFoundation\Actions\FileServeAction::class
                , [ 'baseDir' => PT_DIR_DATA . '/statics' ]
            ))
        ],
    ],

    ## Override Home; redirect to login page
    #
    'home'  => [
        'route' => RouteSegment::class,
        'allow_override' => true, # default is true
        'options' => [
            'criteria'    => '/',
            'match_whole' => true,
        ],
        'params'  => [
            ListenerDispatch::ACTIONS => function() {
                return [
                    ListenerDispatchResult::RESULT_DISPATCH => new ResponseRedirect(
                        \Module\HttpFoundation\Actions::url('main/auth/login')
                    )
                ];
            },
        ],
    ],

    ## Authorization; login
    #
    'auth'  => [
        'route'    => RouteSegment::class,
        'priority' => 1000,
        'options' => [
            'criteria'    => '/auth',
            'match_whole' => false,
        ],
        'routes' => [
            'login' => [
                'route' => RouteSegment::class,
                'options' => [
                    'criteria' => '/',
                ],
                'params' => [
                    ListenerDispatch::ACTIONS => \Module\AdcashAsm\Actions\Auth\LoginAction::class,
                ],
            ],
            'logout' => [
                'route' => RouteSegment::class,
                'options' => [
                    'criteria' => '/logout',
                ],
                'params' => [
                    ListenerDispatch::ACTIONS => \Module\AdcashAsm\Actions\Auth\LogoutAction::class,
                ],
            ],
        ],
    ],

    ## Order Management
    #
    'panel'  => [
        'route' => RouteSegment::class,
        'options' => [
            'criteria'    => '/panel',
            'match_whole' => false,
        ],
        'routes' => [
            'dashboard' => [
                'route'   => RouteSegment::class,
                'options' => [
                    'criteria' => '/',
                    'match_whole' => true,
                ],
                'params'  => [
                    ListenerDispatch::ACTIONS => [

                    ],
                ],
            ], # end dashboard
            'orders' => [
                'route'   => RouteSegment::class,
                'options' => [
                    'criteria' => '/orders',
                    'match_whole' => false,
                ],
                'routes' => [
                    'all' => [
                        'route'   => RouteSegment::class,
                        'options' => [
                            'criteria' => '/',
                        ],
                        'params'  => [
                            ListenerDispatch::ACTIONS => [
                                \Module\AdcashAsm\Actions\Orders\AllOrdersAction::class,
                            ],
                        ],
                    ],
                    'new' => [
                        'route'   => RouteSegment::class,
                        'options' => [
                            'criteria' => '/new',
                        ],
                        'params'  => [
                            ListenerDispatch::ACTIONS => [
                                \Module\AdcashAsm\Actions\Orders\NewOrderAction::class,
                            ],
                        ],
                    ],
                    'edit' => [
                        'route'   => RouteSegment::class,
                        'options' => [
                            'criteria' => '/:orderId~\d+~/edit',
                        ],
                        'params'  => [
                            ListenerDispatch::ACTIONS => [
                                \Module\AdcashAsm\Actions\Orders\EditOrderAction::class,
                            ],
                        ],
                    ],
                    'delete' => [
                        'route'   => RouteSegment::class,
                        'options' => [
                            'criteria' => '/:orderId~\d+~/delete',
                        ],
                        'params'  => [
                            ListenerDispatch::ACTIONS => [
                                \Module\AdcashAsm\Actions\Orders\DeleteOrderAction::class,
                            ],
                        ],
                    ],
                ],
            ], # end order
            'customers' => [
                'route'   => RouteSegment::class,
                'options' => [
                    'criteria' => '/customers',
                    'match_whole' => false,
                ],
                'routes' => [
                    'search' => [
                        'route'   => RouteSegment::class,
                        'options' => [
                            'criteria' => '/',
                        ],
                        'params'  => [
                            ListenerDispatch::ACTIONS => [
                                \Module\AdcashAsm\Actions\Customers\SearchCustomersAction::class,
                            ],
                            // restrict other responses (render strategy) except of json for this route.
                            RenderRouterStrategy::ConfRouteParam => [
                                'strategy' => 'json',
                            ],
                        ],
                    ],
                ],
            ], # end customers
            'products' => [
                'route'   => RouteSegment::class,
                'options' => [
                    'criteria' => '/products',
                    'match_whole' => false,
                ],
                'routes' => [
                    'search' => [
                        'route'   => RouteSegment::class,
                        'options' => [
                            'criteria' => '/',
                        ],
                        'params'  => [
                            ListenerDispatch::ACTIONS => [
                                \Module\AdcashAsm\Actions\Products\SearchProductsAction::class,
                            ],
                            // restrict other responses (render strategy) except of json for this route.
                            RenderRouterStrategy::ConfRouteParam => [
                                'strategy' => 'json',
                            ],
                        ],
                    ],
                ],
            ], # end products
        ],
    ],
];
