<?php
use Module\AdcashAsm\Services\PdoClientService;

return [
    PdoClientService::class => [
        'clients' => [
            'master' => [
                'db'     => \Poirot\getEnv('PDO_DB'),
                'user'   => \Poirot\getEnv('PDO_USER'),
                'pass'   => \Poirot\getEnv('PDO_PASS'),
                'server' => \Poirot\getEnv('PDO_SERVER') ?: 'mariadb',
            ],
        ],
    ],
];
