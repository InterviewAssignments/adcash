<?php
use Module\AdcashAsm\Authorization;
use Module\AdcashAsm\Interfaces;
use Module\AdcashAsm\Repositories;
use Module\AdcashAsm\Services;

return [
    'implementations' => [
        Services::PdoClient => \PDO::class,
    ],
    'services' =>  [
        Services::PdoClient => Services\PdoClientService::class,
    ],
    'nested' => [
        'authorization' => [
            'services' => [
                Authorization\Services::AuthIdentifier    => Authorization\IdentifierSessionService::class,
                Authorization\Services::CredentialAdapter => Authorization\CredentialUserPassAdapter::class,
            ],
        ],
        'repositories' => [
            'implementations' => [
                Services\Repositories::UsersRepo     => Interfaces\Repositories\iRepoUsers::class,
                Services\Repositories::CustomersRepo => Interfaces\Repositories\iRepoCustomers::class,
                Services\Repositories::ProductsRepo  => Interfaces\Repositories\iRepoProducts::class,
                Services\Repositories::OrdersRepo    => Interfaces\Repositories\iRepoOrders::class,
            ],
            'services' => [
                Services\Repositories::UsersRepo     => Repositories\Driver\Mysql\UsersRepo::class,
                Services\Repositories::CustomersRepo => Repositories\Driver\Mysql\CustomersRepo::class,
                Services\Repositories::ProductsRepo  => Repositories\Driver\Mysql\ProductsRepo::class,
                Services\Repositories::OrdersRepo    => Repositories\Driver\Mysql\OrdersRepo::class,
            ],
        ],
    ],
];
