<?php
namespace Module\AdcashAsm\Authorization;

use Poirot\AuthSystem\Authenticate\Identifier\IdentifierSession as BaseIdentifierSession;
use Poirot\AuthSystem\Authenticate\Interfaces\iIdentity;


class IdentifierSession
    extends BaseIdentifierSession
{
    /**
     * @inheritdoc
     */
    function giveIdentity(iIdentity $identity)
    {
        if (! $identity instanceof IdentityAdcashUser)
            throw new \LogicException(
                'Adcash Identifier Could Only Work With Default Identity instance of IdentityAdcashUser.'
            );

        parent::giveIdentity($identity);
    }

    /**
     * Get Default Identity Instance
     * that Signed data load into
     *
     * @return IdentityAdcashUser
     */
    protected function _newDefaultIdentity()
    {
        $identity = new IdentityAdcashUser;
        return $identity;
    }
}
