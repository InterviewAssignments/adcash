<?php
namespace Module\AdcashAsm\Authorization;

use Module\AdcashAsm\Authorization\IdentifierSession;
use Module\AdcashAsm\Module;
use Poirot\AuthSystem\Authenticate\Exceptions\AuthenticationError;
use Poirot\Ioc\Container\Service\aServiceContainer;


class IdentifierSessionService
    extends aServiceContainer
{
    /**
     * @inheritdoc
     *
     * @return IdentifierSession
     * @throws \Exception
     */
    function newService()
    {
        $identifier = (new IdentifierSession(Module::AUTH_REALM))
            ->setIssuerException($this->funcAccessDeniedIssuer());

        return $identifier;
    }

    // ..

    private function funcAccessDeniedIssuer()
    {
        ## Attain Login Continue If Has
        #
        return function(AuthenticationError $e)
        {
            $continue = (string) \Module\HttpFoundation\Actions::url(); # current requested page

            $loginUrl = \Module\HttpFoundation\Actions::url('main/auth/login'); // ensure routes loaded
            $loginUrl = \Poirot\Psr7\modifyUri( $loginUrl->uri(), [ 'query' => ['continue' => $continue] ] );
            header('Location: '.$loginUrl);
            die; # header sent but page not redirected unless we die progress here..
        };
    }
}
