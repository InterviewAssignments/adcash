<?php
namespace Module\AdcashAsm\Authorization;

use Module\AdcashAsm\Interfaces\Repositories\iRepoUsers;
use Poirot\AuthSystem\Authenticate\Interfaces\iIdentity;
use Poirot\AuthSystem\Authenticate\RepoIdentityCredential\aIdentityCredentialAdapter;
use Poirot\Std\Exceptions\UnexpectedInputValueError;


/**
 * Authenticate User/Pass Credential Adapter
 *
 */
class CredentialUserPassAdapter
    extends aIdentityCredentialAdapter
{
    /** @var iRepoUsers */
    protected $repoUsers;

    protected $username;
    protected $password;


    /**
     * CredentialUserPassAdapter
     *
     * @param iRepoUsers        $repoUsers @IoC /module/AdCashAsm/services/repositories/UsersRepo
     * @param null|\Traversable $options
     */
    function __construct(iRepoUsers $repoUsers, $options = null)
    {
        $this->repoUsers = $repoUsers;

        parent::__construct($options);
    }


    /**
     * Do Match Identity With Given Options/Credential
     *
     * @param array $credentials Include Credential Data
     *
     * @return iIdentity|false
     */
    protected function doFindIdentityMatch(array $credentials)
    {
        $username = $credentials['username'];
        $password = $credentials['password'];
        if (!isset($username) || !isset($password))
            throw new UnexpectedInputValueError('Identifier / Credential Is Missing on Credential Adapter.');

        if (! $user = $this->repoUsers->findOneByUserPass($username, $password) )
            return false;

        return (new IdentityAdcashUser())
            ->setOwnerId($user->getUid())
            ->setMetaData([
                'username' => $user->getUsername(),
                'fullname' => $user->getFullname(),
            ]);
    }


    // Credentials as Options:

    /**
     * @required
     *
     * @return string
     */
    function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    function setUsername($username)
    {
        $this->username = (string) $username;
        return $this;
    }

    /**
     * @return string
     */
    function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    function setPassword($password)
    {
        $this->password = (string) $password;
        return $this;
    }
}
