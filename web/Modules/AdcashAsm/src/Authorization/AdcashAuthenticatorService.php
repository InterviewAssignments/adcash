<?php
namespace Module\AdcashAsm\Authorization;

use Module\AdcashAsm\Module;
use Module\Authorization\Services\AuthenticatorPlugins;
use Poirot\AuthSystem\Authenticate\Authenticator;
use Poirot\AuthSystem\Authenticate\Identifier\IdentifierSession;
use Poirot\AuthSystem\Authenticate\Interfaces\iIdentifier;
use Poirot\Ioc\Container\Service\aServiceContainer;


class AdcashAuthenticatorService
    extends aServiceContainer
{
    protected $name = Module::AUTH_REALM;


    /**
     * @inheritdoc
     *
     * @return Authenticator
     * @throws \Exception
     */
    function newService()
    {
        ## Set Credential Repo Adapter
        #
        return new Authenticator(
            $this->_getIdentifier()
            , $this->_credentialAdapter()
        );
    }

    // ..

    /**
     * Identifier Authenticator
     *
     * @return iIdentifier|IdentifierSession
     * @throws \Exception
     */
    protected function _getIdentifier()
    {
        $identifier = $this->services()
            ->from('/module/AdcashAsm/services/authorization')
            ->get(Services::AuthIdentifier);

        return $identifier;
    }

    /**
     * Credential Adapter
     *
     * @return CredentialUserPassAdapter
     * @throws \Exception
     */
    protected function _credentialAdapter()
    {
        $credentialAdapter = $this->services()
            ->from('/module/AdcashAsm/services/authorization')
            ->get(Services::CredentialAdapter);

        return $credentialAdapter;
    }

    // Implement Service Aware:

    /**
     * @override
     * note: Access Only In Capped Collection; No Nested Containers Here
     *
     * @return AuthenticatorPlugins
     */
    function services()
    {
        return parent::services();
    }
}
