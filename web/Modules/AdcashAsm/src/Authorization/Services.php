<?php
/*
 * Facade Class To Easy Access To Services
 *
 */
namespace Module\AdcashAsm\Authorization
{
    /**
     * @method static CredentialUserPassAdapter UserPassCredentialAdapter()
     */
    class Services extends \IOC
    {
        const AuthIdentifier    = 'AuthIdentifier';
        const CredentialAdapter = 'CredentialAdapter';
    }
}
