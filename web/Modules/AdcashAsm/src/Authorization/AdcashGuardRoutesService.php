<?php
namespace Module\AdcashAsm\Authorization;

use Module\AdcashAsm\Module;
use Module\Authorization\Services\Guards\aGuardService;
use Module\Authorization\Guard\GuardRoute;


class AdcashGuardRoutesService
    extends aGuardService
{
    /** @var string Service Name */
    protected $name = 'adcash.auth.guard.routes';
    protected $authenticatorName = Module::AUTH_REALM;


    /**
     * @inheritdoc
     * @return GuardRoute
     */
    function newService()
    {
        $guard = new GuardRoute;
        $guard->setAuthenticator( $this->_attainAuthenticatorByName() );
        $guard->setRoutesDenied([
            'main/panel/*',
        ]);

        return $guard;
    }
}
