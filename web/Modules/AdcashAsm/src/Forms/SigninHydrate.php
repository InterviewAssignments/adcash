<?php
namespace Module\AdcashAsm\Forms;

use Poirot\Std\Exceptions\UnexpectedInputValueError;


class SigninHydrate
    extends AbstractHydrate
{
    protected $identifier;
    protected $credential;


    // Implement Validator:

    /**
     * Do Assertion Validate and Return An Array Of Errors
     *
     * @return UnexpectedInputValueError[]
     */
    function doAssertValidate()
    {
        $exceptions = [];

        ## Validate Identifier
        #
        if ( null === $this->getIdentifier() )
            $exceptions[] = UnexpectedInputValueError::paramIsRequired('identifier');

        ## Validate/Assert Credential
        #
        $credential = $this->getCredential();
        if ( empty($credential) )
            $exceptions[] = UnexpectedInputValueError::paramIsRequired('credential');
        else if (strlen($credential) < 5)
            $exceptions[] = UnexpectedInputValueError::lessThanMinLength('credential');


        return $exceptions;
    }

    // Setter Options:

    /**
     * Identifier
     *
     * @param mixed $identifier
     */
    function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * Password
     *
     * @param mixed $credential
     */
    function setCredential($credential)
    {
        $this->credential = $credential;
    }

    // Hydration Getters:

    /**
     * @return string
     */
    function getIdentifier()
    {
        $identifier = $this->_assertNewLine( $this->_assertTrim($this->identifier) );
        if ( empty($identifier) )
            return null;

        return $identifier;
    }

    /**
     * @return string
     */
    function getCredential()
    {
        $credential = $this->_assertNewLine( $this->_assertTrim($this->credential) );
        return $credential;
    }
}
