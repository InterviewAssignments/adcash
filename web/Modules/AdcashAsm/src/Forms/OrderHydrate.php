<?php
namespace Module\AdcashAsm\Forms;

use Poirot\Std\Exceptions\UnexpectedInputValueError;


class OrderHydrate
    extends AbstractHydrate
{
    protected $customer;
    protected $products;
    protected $productQuantities;


    // Implement Validator:

    /**
     * Do Assertion Validate and Return An Array Of Errors
     *
     * @return UnexpectedInputValueError[]
     */
    function doAssertValidate()
    {
        $exceptions = [];

        if ( null === $this->getCustomer() )
            $exceptions[] = UnexpectedInputValueError::paramIsRequired('customer');

        $products = $this->getProducts();
        if ( empty($products) )
            $exceptions[] = UnexpectedInputValueError::paramIsRequired('products');
        elseif (! is_array($products) )
            $exceptions[] = UnexpectedInputValueError::error('products');

        $quantities = $this->getProductQuantities();
        if ( empty($quantities) )
            $exceptions[] = UnexpectedInputValueError::paramIsRequired('product_quantities');
        elseif (! is_array($quantities) )
            $exceptions[] = UnexpectedInputValueError::error('product_quantities');

        if (count($products) != count($quantities))
            $exceptions[] = UnexpectedInputValueError::error('product_quantities'
                , 'Products & Quantities are not Match.');


        return $exceptions;
    }


    // Setter Options:

    /**
     * Customer
     *
     * @param $value
     */
    function setCustomer($value)
    {
        $this->customer = $value;
    }

    /**
     * Product Identifier
     *
     * @param $value
     */
    function setProducts($value)
    {
        $this->products = $value;
    }

    /**
     * Product Quantity
     *
     * @param $value
     */
    function setProductQuantities($value)
    {
        $this->productQuantities = $value;
    }


    // Hydration Getters:

    /**
     * @return int
     */
    function getCustomer()
    {
        return $this->customer ? (int) $this->customer : null;
    }

    function getProducts()
    {
        return $this->products ?? null;
    }

    function getProductQuantities()
    {
        return $this->productQuantities ?? null;
    }
}
