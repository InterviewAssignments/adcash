<?php
namespace Module\AdcashAsm\Forms;

use Poirot\Std\Hydrator\aHydrateEntity;
use Poirot\Std\Interfaces\Pact\ipValidator;
use Poirot\Std\tValidator;


abstract class AbstractHydrate
    extends aHydrateEntity
    implements ipValidator
{
    use tValidator;


    protected function _assertNewLine($description)
    {
        return preg_replace( '/\t+/', '',  preg_replace('/(\r\n|\n|\r){3,}/', "$1$1", $description) );
    }

    protected function _assertTrim($description)
    {
        return trim($description);
    }
}
