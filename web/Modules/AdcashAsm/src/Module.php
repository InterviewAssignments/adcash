<?php
namespace Module\AdcashAsm
{
    use Module\AdcashAsm\Authorization\AdcashAuthenticatorService;
    use Module\AdcashAsm\Authorization\AdcashGuardRoutesService;
    use Module\Themes\Sapi\Feature\iFeatureModuleEnableThemes;
    use Poirot\Application\Interfaces\Sapi;
    use Poirot\Application\Interfaces\Sapi\iSapiModule;
    use Poirot\Application\ModuleManager\Interfaces\iModuleManager;
    use Poirot\Ioc\Container;
    use Poirot\Ioc\Container\BuildContainer;
    use Poirot\Loader\Autoloader\LoaderAutoloadAggregate;
    use Poirot\Loader\Autoloader\LoaderAutoloadNamespace;
    use Poirot\Router\BuildRouterStack;
    use Poirot\Router\Interfaces\iRouterStack;
    use Poirot\Std\Interfaces\Struct\iDataEntity;


    class Module implements iSapiModule
        , Sapi\Module\Feature\iFeatureModuleAutoload
        , Sapi\Module\Feature\iFeatureModuleInitModuleManager
        , Sapi\Module\Feature\iFeatureModuleMergeConfig
        , Sapi\Module\Feature\iFeatureModuleNestServices
        , Sapi\Module\Feature\iFeatureModuleNestActions
        , Sapi\Module\Feature\iFeatureOnPostLoadModulesGrabServices
        , iFeatureModuleEnableThemes
    {
        // we might have different realms for authentication
        // for instance API, administration, etc..
        const AUTH_REALM = 'adcash.authenticator.default';


        /**
         * @inheritdoc
         */
        function initAutoload(LoaderAutoloadAggregate $baseAutoloader)
        {
            /** @var LoaderAutoloadNamespace $nameSpaceLoader */
            $nameSpaceLoader = $baseAutoloader->loader(LoaderAutoloadNamespace::class);
            $nameSpaceLoader->addResource(__NAMESPACE__, __DIR__);


            include_once __DIR__.'/_functions.php';
        }

        /**
         * @inheritdoc
         */
        function initModuleManager(iModuleManager $moduleManager)
        {
            ## Load Required Modules
            #
            if (! $moduleManager->hasLoaded('Authorization') )
                $moduleManager->loadModule('Authorization');

            if (! $moduleManager->hasLoaded('Themes') )
                $moduleManager->loadModule('Themes');

        }

        /**
         * @inheritdoc
         */
        function initConfig(iDataEntity $config)
        {
            return \Poirot\Config\load(__DIR__ . '/../conf/mod-adcash_asm');
        }

        /**
         * @inheritdoc
         */
        function getServices(Container $moduleContainer = null)
        {
            $conf = include __DIR__ . '/../conf/services.conf.php';
            return new BuildContainer($conf);
        }

        /**
         * @inheritdoc
         */
        function getActions()
        {
            return include_once __DIR__ . '/../conf/actions.conf.php';
        }

        /**
         * @inheritdoc
         * @param iRouterStack $router
         */
        function resolveRegisteredServices($router = null)
        {
            # Register Http Routes:
            #
            if ( $router ) {
                $routes = include __DIR__ . '/../conf/routes.conf.php';
                $buildRoute = new BuildRouterStack;
                $buildRoute->setRoutes($routes);
                $buildRoute->build($router);
            }

            ## Authorization / Guards
            #
            \Module\Authorization\Services::AuthenticatorPlugins()
                ->set(new AdcashAuthenticatorService);

            \Module\Authorization\Services::GuardPlugins()
                ->set(new AdcashGuardRoutesService);

        }

        // Implement iFeatureModuleEnableThemes:

        /**
         * Return Available Theme Directories
         *
         * @return array Path to theme directory
         */
        function registerAvailableThemes()
        {
            return [
                __DIR__ . '/../theme'
            ];
        }
    }
}
