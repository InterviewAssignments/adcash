<?php
namespace Module\AdcashAsm\Repositories\Driver\Mysql;

use Module\AdcashAsm\Interfaces\Entities\iProductEntity;
use Module\AdcashAsm\Interfaces\Repositories\iRepoProducts;
use Module\AdcashAsm\Repositories\Entities\ProductEntity;
use Poirot\Std\GeneratorWrapper;
use Poirot\ValueObjects\Amount\AmountObject;


class ProductsRepo
    implements iRepoProducts
{
    /** @var \PDO */
    protected $pdo;
    protected $table = 'products';


    /**
     * UserRepo
     *
     * @param \PDO $pdoDriver
     */
    function __construct(\PDO $pdoDriver)
    {
        $this->pdo = $pdoDriver;
    }


    /**
     * Find Entity By Given UID
     *
     * @param mixed $uid
     *
     * @return iProductEntity|null
     */
    function findOneByUID($uid)
    {
        $query = "SELECT * FROM {$this->table} WHERE product_id = '{$uid}'";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute([$uid]);
        if (! $customer = $stmt->fetch(\PDO::FETCH_ASSOC))
            return null;

        return (new ProductEntity())
            ->setUid($customer['product_id'])
            ->setTitle($customer['title'])
            ->setDescription($customer['description'])
            ->setThumbUrl($customer['thumb'])
            ->setPrice(new AmountObject([
                'value'    => $customer['price'],
                'currency' => $customer['currency']]))
            ->setDateCreated(new \DateTime($customer['created_date']))
            ->setDateUpdated(!empty($customer['updated_date']) ? $customer['updated_date'] : null)
            ->setAvailable($customer['is_available'])
        ;
    }

    /**
     * Find All Products
     *
     * @param int|null $limit
     * @param null $offset
     *
     * @return iProductEntity[]
     */
    function findAll(?int $limit = null, $offset = null)
    {
        $query = "SELECT * FROM {$this->table}";
        $binds = [];
        if (null !== $limit) {
            $query .= " LIMIT {$limit}";
            $binds[] = $limit;
        }
        if (null !== $offset) {
            $offset = (int) $offset;
            $query .= ", $offset";
            $binds[] = $offset;
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($binds);
        $customers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return new GeneratorWrapper($customers, function ($value, $_) {
            return (new ProductEntity())
                ->setUid($value['product_id'])
                ->setTitle($value['title'])
                ->setDescription($value['description'])
                ->setThumbUrl($value['thumb'])
                ->setPrice(new AmountObject([
                    'value'    => $value['price'],
                    'currency' => $value['currency']]))
                ->setDateCreated(new \DateTime($value['created_date']))
                ->setDateUpdated(!empty($value['updated_date']) ? $value['updated_date'] : null)
                ->setAvailable($value['is_available'])
            ;
        });
    }

    /**
     * Find All Available Products
     *
     * @param int|null $limit
     * @param null $offset
     *
     * @return iProductEntity[]
     */
    function findAllAvailable(?int $limit = null, $offset = null)
    {
        $query = "SELECT * FROM {$this->table} WHERE is_available = 1";
        $binds = [];
        if (null !== $limit) {
            $query .= " LIMIT {$limit}";
            $binds[] = $limit;
        }
        if (null !== $offset) {
            $offset = (int) $offset;
            $query .= ", $offset";
            $binds[] = $offset;
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($binds);
        $customers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return new GeneratorWrapper($customers, function ($value, $_) {
            return (new ProductEntity())
                ->setUid($value['product_id'])
                ->setTitle($value['title'])
                ->setDescription($value['description'])
                ->setThumbUrl($value['thumb'])
                ->setPrice(new AmountObject([
                    'value'    => $value['price'],
                    'currency' => $value['currency']]))
                ->setDateCreated(new \DateTime($value['created_date']))
                ->setDateUpdated(!empty($value['updated_date']) ? $value['updated_date'] : null)
                ->setAvailable($value['is_available'])
            ;
        });
    }

    /**
     * Find All Has Name Like Given Name
     *
     * @param string $name
     * @param int|null $limit
     * @param null $offset
     *
     * @return iProductEntity[]
     */
    function findAllAvailableWithName($name, ?int $limit = null, $offset = null)
    {
        $query = "SELECT * FROM {$this->table} WHERE fullname like '%{$name}%'";
        $binds = [];
        if (null !== $limit) {
            $query .= "LIMIT {$limit}";
            $binds[] = $limit;
        }
        if (null !== $offset) {
            $offset = (int) $offset;
            $query .= ", $offset";
            $binds[] = $offset;
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($binds);
        $customers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return new GeneratorWrapper($customers, function ($value, $_) {
            return (new ProductEntity())
                ->setUid($value['product_id'])
                ->setTitle($value['title'])
                ->setDescription($value['description'])
                ->setThumbUrl($value['thumb'])
                ->setPrice(new AmountObject([
                    'value'    => $value['price'],
                    'currency' => $value['currency']]))
                ->setDateCreated(new \DateTime($value['created_date']))
                ->setDateUpdated(!empty($value['updated_date']) ? $value['updated_date'] : null)
                ->setAvailable($value['is_available'])
            ;
        });
    }
}
