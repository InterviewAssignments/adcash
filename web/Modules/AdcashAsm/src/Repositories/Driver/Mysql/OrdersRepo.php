<?php
namespace Module\AdcashAsm\Repositories\Driver\Mysql;

use Module\AdcashAsm\Interfaces\Entities\iCustomerEntity;
use Module\AdcashAsm\Interfaces\Entities\iOrderEntity;
use Module\AdcashAsm\Interfaces\Entities\iProductEntity;
use Module\AdcashAsm\Interfaces\Repositories\iRepoCustomers;
use Module\AdcashAsm\Interfaces\Repositories\iRepoOrders;
use Module\AdcashAsm\Repositories\Entities\OrderEntity;
use Module\AdcashAsm\Services\Repositories;
use Poirot\Std\GeneratorWrapper;
use Poirot\ValueObjects\Amount\AmountObject;


class OrdersRepo
    implements iRepoOrders
{
    /** @var \PDO */
    protected $pdo;
    /** @var iRepoCustomers */
    protected $repoCustomers;

    protected $table = 'factors';
    protected $table_products = 'factors_products';
    protected $table_promotions = 'factors_promotions';


    /**
     * OrdersRepo
     *
     * @param \PDO $pdoDriver
     * @param iRepoCustomers $repoCustomers @IoC /module/AdcashAsm/services/repositories/CustomersRepo
     */
    function __construct(\PDO $pdoDriver, iRepoCustomers $repoCustomers)
    {
        $this->pdo = $pdoDriver;
        $this->repoCustomers = $repoCustomers;
    }


    /**
     * Find Entity By Given UID
     *
     * @param mixed $uid
     *
     * @return iOrderEntity|null
     * @throws \Exception
     */
    function findOneByUID($uid)
    {
        $query = "SELECT * FROM {$this->table} WHERE order_id = '{$uid}'";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute([$uid]);
        if (! $order = $stmt->fetch(\PDO::FETCH_ASSOC))
            return null;

        return (new OrderEntity())
            ->setUid($order['order_id'])
            ->setStatus($order['status'])
            ->setCustomer($this->repoCustomers->findOneByUID($order['customer_id']))
            ->setCountItems($order['items_count'])
            ->setCountTotalQuantities($order['quantities_total'])
            ->setTotalPrice(new AmountObject([
                'value'    => $order['price_total'],
                'currency' => $order['currency']]))
            ->setTotalPromoPrice(new AmountObject([
                'value' => $order['price_promotions'],
                'currency' => $order['currency']]))
            ->setDateCreated(new \DateTime($order['created_date']));
    }

    /**
     * Find All Orders
     *
     * @param int|null $limit
     * @param null     $offset
     * @param int      $order
     *
     * @return iOrderEntity[]
     */
    function findAll(?int $limit = null, $offset = null, $order = self::ORDER_ASC)
    {
        $query = "SELECT * FROM {$this->table}";
        $query.= " ORDER BY order_id " . ($order === self::ORDER_ASC ? 'ASC' : 'DESC');
        $binds = [];
        if (null !== $limit) {
            $query .= " LIMIT {$limit}";
            $binds[] = $limit;
        }
        if (null !== $offset) {
            $offset = (int) $offset;
            $query .= ", $offset";
            $binds[] = $offset;
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($binds);
        $customers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return new GeneratorWrapper($customers, function ($value, $_) {
            return (new OrderEntity())
                ->setUid($value['order_id'])
                ->setStatus($value['status'])
                // It's possible to define Row Object that is aware of mysql(pdo) driver
                // and implement kind of lazy loading here.
                ->setCustomer($this->repoCustomers->findOneByUID($value['customer_id']))
                ->setCountItems($value['items_count'])
                ->setCountTotalQuantities($value['quantities_total'])
                ->setTotalPrice(new AmountObject([
                    'value'    => $value['price_total'],
                    'currency' => $value['currency']]))
                ->setTotalPromoPrice(new AmountObject([
                    'value' => $value['price_promotions'],
                    'currency' => $value['currency']]))
                ->setDateCreated(new \DateTime($value['created_date']));
        });
    }

    /**
     * Find All Orders After Given Date
     *
     * @param \DateTime $date
     * @param int|null $limit
     * @param null $offset
     * @param int $order
     *
     * @return iOrderEntity[]|\Traversable
     */
    function findAllAfterDate(?\DateTime $date = null, ?int $limit = null, $offset = null, $order = self::ORDER_ASC)
    {
        if ($date === null)
            return $this->findAll($limit, $offset, $order);

        $dateIso = $date->format('Y-m-d');
        $query = "SELECT * FROM {$this->table} WHERE created_date > '$dateIso'";
        $query.= " ORDER BY order_id " . ($order === self::ORDER_ASC ? 'ASC' : 'DESC');
        $binds = [];
        if (null !== $limit) {
            $query .= " LIMIT {$limit}";
            $binds[] = $limit;
        }
        if (null !== $offset) {
            $offset = (int) $offset;
            $query .= ", $offset";
            $binds[] = $offset;
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($binds);
        $customers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return new GeneratorWrapper($customers, function ($value, $_) {
            return (new OrderEntity())
                ->setUid($value['order_id'])
                ->setStatus($value['status'])
                // It's possible to define Row Object that is aware of mysql(pdo) driver
                // and implement kind of lazy loading here.
                ->setCustomer($this->repoCustomers->findOneByUID($value['customer_id']))
                ->setCountItems($value['items_count'])
                ->setCountTotalQuantities($value['quantities_total'])
                ->setTotalPrice(new AmountObject([
                    'value'    => $value['price_total'],
                    'currency' => $value['currency']]))
                ->setTotalPromoPrice(new AmountObject([
                    'value' => $value['price_promotions'],
                    'currency' => $value['currency']]))
                ->setDateCreated(new \DateTime($value['created_date']));
        });
    }

    /**
     * Find All Orders Owned By Given Customer ID
     *
     * @param $customerUid
     * @param \DateTime|null $date
     * @param int|null $limit
     * @param null $offset
     * @param int $order
     *
     * @return iOrderEntity[]|\Traversable
     */
    function findAllOwnedByCustomer($customerUid, ?\DateTime $date = null, ?int $limit = null, $offset = null, $order = self::ORDER_ASC)
    {
        $query = "SELECT * FROM {$this->table} WHERE customer_id = '$customerUid'";

        if ($date !== null) {
            $dateIso = $date->format('Y-m-d');
            $query.= " AND created_date > '$dateIso'";
        }

        $query.= " ORDER BY order_id " . ($order === self::ORDER_ASC ? 'ASC' : 'DESC');
        $binds = [];
        if (null !== $limit) {
            $query .= " LIMIT {$limit}";
            $binds[] = $limit;
        }
        if (null !== $offset) {
            $offset = (int) $offset;
            $query .= ", $offset";
            $binds[] = $offset;
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($binds);
        $customers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return new GeneratorWrapper($customers, function ($value, $_) {
            return (new OrderEntity())
                ->setUid($value['order_id'])
                ->setStatus($value['status'])
                // It's possible to define Row Object that is aware of mysql(pdo) driver
                // and implement kind of lazy loading here.
                ->setCustomer($this->repoCustomers->findOneByUID($value['customer_id']))
                ->setCountItems($value['items_count'])
                ->setCountTotalQuantities($value['quantities_total'])
                ->setTotalPrice(new AmountObject([
                    'value'    => $value['price_total'],
                    'currency' => $value['currency']]))
                ->setTotalPromoPrice(new AmountObject([
                    'value' => $value['price_promotions'],
                    'currency' => $value['currency']]))
                ->setDateCreated(new \DateTime($value['created_date']));
        });
    }

    /**
     * Find All Orders That Has Given Product Id
     *
     * @param $productId
     * @param \DateTime|null $date
     * @param int|null $limit
     * @param null $offset
     * @param int $order
     *
     * @return iOrderEntity[]|\Traversable
     */
    function findAllThatHasProduct($productId, ?\DateTime $date = null, ?int $limit = null, $offset = null, $order = self::ORDER_ASC)
    {
        $query = "SELECT * FROM `{$this->table}`
          LEFT JOIN factors_products on factors_products.order_id = factors.order_id
          WHERE factors_products.product_id = '$productId'";

        if ($date !== null) {
            $dateIso = $date->format('Y-m-d');
            $query.= " AND factors.created_date > '$dateIso'";
        }

        $query.= " ORDER BY factors.order_id " . ($order === self::ORDER_ASC ? 'ASC' : 'DESC');
        $binds = [];
        if (null !== $limit) {
            $query .= " LIMIT {$limit}";
            $binds[] = $limit;
        }
        if (null !== $offset) {
            $offset = (int) $offset;
            $query .= ", $offset";
            $binds[] = $offset;
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($binds);
        $customers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return new GeneratorWrapper($customers, function ($value, $_) {
            return (new OrderEntity())
                ->setUid($value['order_id'])
                ->setStatus($value['status'])
                // It's possible to define Row Object that is aware of mysql(pdo) driver
                // and implement kind of lazy loading here.
                ->setCustomer($this->repoCustomers->findOneByUID($value['customer_id']))
                ->setCountItems($value['items_count'])
                ->setCountTotalQuantities($value['quantities_total'])
                ->setTotalPrice(new AmountObject([
                    'value'    => $value['price_total'],
                    'currency' => $value['currency']]))
                ->setTotalPromoPrice(new AmountObject([
                    'value' => $value['price_promotions'],
                    'currency' => $value['currency']]))
                ->setDateCreated(new \DateTime($value['created_date']));
        });
    }

    /**
     * New Order With Products
     *
     * @param iCustomerEntity $customer
     * @param array           $productList ['product' => iProductEntity, ('count' => 2)]
     *
     * @return iOrderEntity Persist entity with uid
     * @throws \Exception
     */
    function insertNewOrderAndProducts(iCustomerEntity $customer, array $productList)
    {
        $quantitiesTotal = 0;
        $countItems      = 0;
        $totalPrice      = new AmountObject;
        foreach ($productList as $pr)
        {
            /** @var iProductEntity $product */
            $product = $pr['product'];
            $count   = $pr['count'] ?? 1;

            $countItems++; // could be more accurate
            $quantitiesTotal += $count;
            $totalPrice->add($product->getPrice(), $count);
        }

        ## Insert Order Query
        #
        $this->pdo->beginTransaction();
        try {
            $sql = "INSERT INTO `{$this->table}` 
                    (`customer_id`, `price_promotions`, `price_total`, `currency`, `items_count`, 
                     `quantities_total`, `status`, `created_date`)
                    VALUES (:customer_id, :price_promotions, :price_total, :currency, :items_count, 
                            :quantities_total, :status, now());";

            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                'customer_id'      => $customer->getUid(),
                'price_promotions' => null,
                'price_total'      => $totalPrice->getValue(),
                'currency'         => $totalPrice->getCurrency(),
                'items_count'      => $countItems,
                'quantities_total' => $quantitiesTotal,
                'status'           => iOrderEntity::STATUS_UNPAID,
            ]);

            $uid = $this->pdo->lastInsertId();

            ### Save Products
            #
            foreach ($productList as $pr) {
                /** @var iProductEntity $product */
                $product = $pr['product'];
                $count   = $pr['count'] ?? 1;

                $sql = "INSERT INTO `{$this->table_products}` 
                    (`order_id`, `product_id`, `quantity`, `price`, `price_total`, `currency`)
                    VALUES (:order_id, :product_id, :quantity, :price, :price_total, :currency);";

                $stmt = $this->pdo->prepare($sql);
                $stmt->execute([
                    'order_id'    => $uid,
                    'product_id'  => $product->getUid(),
                    'quantity'    => $count,
                    'price'       => $product->getPrice()->getValue(),
                    'price_total' => $product->getPrice()->getValue() * $count,
                    'currency'    => $product->getPrice()->getCurrency(),
                ]);
            }

        } catch (\Exception $e) {
            $this->pdo->rollBack();
            throw $e;
        }
        $this->pdo->commit();


        $order = new OrderEntity;
        $order
            ->setUid($uid)
            ->setCustomer($customer)
            ->setTotalPrice($totalPrice)
            ->setCountItems($countItems)
            ->setCountTotalQuantities($quantitiesTotal);

        return $order;
    }

    /**
     * Save Changes For Order With Products
     *
     * @param iOrderEntity $order
     * @param array        $productList ['product' => iProductEntity, ('count' => 2)]
     *
     * @return iOrderEntity
     * @throws \Exception
     */
    function updateOrderAndProducts(iOrderEntity $order, array $productList)
    {
        $quantitiesTotal = 0;
        $countItems      = 0;
        $totalPrice      = new AmountObject;
        foreach ($productList as $pr)
        {
            /** @var iProductEntity $product */
            $product = $pr['product'];
            $count   = $pr['count'] ?? 1;

            $countItems++; // could be more accurate
            $quantitiesTotal += $count;
            $totalPrice->add($product->getPrice(), $count);
        }

        ## Insert Order Query
        #
        $this->pdo->beginTransaction();
        try {
            $sql = "UPDATE `{$this->table}` 
                    SET `customer_id`=:customer_id, `price_promotions`=:price_promotions, 
                        `price_total`=:price_total, `currency`=:currency, `items_count`=:items_count, 
                        `quantities_total`=:quantities_total, `status`=:status
                    WHERE order_id=:order_id";

            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                'customer_id'      => $order->getCustomer()->getUid(),
                'price_promotions' => null,
                'price_total'      => $totalPrice->getValue(),
                'currency'         => $totalPrice->getCurrency(),
                'items_count'      => $countItems,
                'quantities_total' => $quantitiesTotal,
                'status'           => $order->getStatus(),
                'order_id'         => $order->getUid(),
            ]);

            $orderId = $order->getUid();
            $stm = $this->pdo->prepare("DELETE FROM {$this->table_promotions} WHERE order_id={$orderId}");
            $stm->execute();

            $stm = $this->pdo->prepare("DELETE FROM {$this->table_products} WHERE order_id={$orderId}");
            $stm->execute();

            ### Save Products
            #
            foreach ($productList as $pr) {
                /** @var iProductEntity $product */
                $product = $pr['product'];
                $count   = $pr['count'] ?? 1;

                $sql = "INSERT INTO `{$this->table_products}` 
                    (`order_id`, `product_id`, `quantity`, `price`, `price_total`, `currency`)
                    VALUES (:order_id, :product_id, :quantity, :price, :price_total, :currency);";

                $stmt = $this->pdo->prepare($sql);
                $stmt->execute([
                    'order_id'    => $orderId,
                    'product_id'  => $product->getUid(),
                    'quantity'    => $count,
                    'price'       => $product->getPrice()->getValue(),
                    'price_total' => $product->getPrice()->getValue() * $count,
                    'currency'    => $product->getPrice()->getCurrency(),
                ]);
            }

        } catch (\Exception $e) {
            $this->pdo->rollBack();
            throw $e;
        }
        $this->pdo->commit();


        $rOrder = new OrderEntity;
        $rOrder
            ->setUid($orderId)
            ->setCustomer($order->getCustomer())
            ->setTotalPrice($totalPrice)
            ->setCountItems($countItems)
            ->setCountTotalQuantities($quantitiesTotal);

        return $rOrder;
    }

    /**
     * Find All Products Belong To Order
     *
     * @param mixed $orderUid
     *
     * @return iProductEntity[]|\Traversable
     */
    function findAllProductsBelongToOrder($orderUid)
    {
        $query = "SELECT * FROM {$this->table_products} WHERE order_id={$orderUid}";

        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $products = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return new GeneratorWrapper($products, function ($value, $_) {
            $product = Repositories::ProductsRepo()
                ->findOneByUID($value['product_id']);

            return $product
                ->setPrice(new AmountObject(['value' => $value['price'], 'currency' => $value['currency']]))
                ->setQuantity($value['quantity'])
            ;
        });
    }

    /**
     * Delete Entity By Given ID
     *
     * @param mixed $orderId
     *
     * @return bool
     * @throws \Exception
     */
    function deleteOneByUid($orderId)
    {
        $this->pdo->beginTransaction();
        try {
            $stm = $this->pdo->prepare("DELETE FROM {$this->table_promotions} WHERE order_id={$orderId}");
            $stm->execute();

            $stm = $this->pdo->prepare("DELETE FROM {$this->table_products} WHERE order_id={$orderId}");
            $stm->execute();

            $stm = $this->pdo->prepare("DELETE FROM {$this->table} WHERE order_id={$orderId}");
            $stm->execute();

        } catch (\Exception $e) {
            $this->pdo->rollBack();
            throw $e;
        }

        $this->pdo->commit();
        return $stm->rowCount();
    }
}
