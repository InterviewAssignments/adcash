<?php
namespace Module\AdcashAsm\Repositories\Driver\Mysql;

use Module\AdcashAsm\Interfaces\Entities\iCustomerEntity;
use Module\AdcashAsm\Interfaces\Repositories\iRepoCustomers;
use Module\AdcashAsm\Repositories\Entities\CustomerEntity;
use Poirot\Std\GeneratorWrapper;


class CustomersRepo
    implements iRepoCustomers
{
    /** @var \PDO */
    protected $pdo;
    protected $table = 'customers';


    /**
     * UserRepo
     *
     * @param \PDO $pdoDriver
     */
    function __construct(\PDO $pdoDriver)
    {
        $this->pdo = $pdoDriver;
    }


    /**
     * Find Entity By Given UID
     *
     * @param mixed $uid
     *
     * @return iCustomerEntity|null
     * @throws \Exception
     */
    function findOneByUID($uid)
    {
        $query = "SELECT * FROM {$this->table} WHERE customer_id = '{$uid}'";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute([$uid]);
        if (! $customer = $stmt->fetch(\PDO::FETCH_ASSOC))
            return null;

        return (new CustomerEntity())
            ->setUid($customer['customer_id'])
            ->setFullname($customer['fullname'])
            ->setDateCreated(new \DateTime($customer['created_date']));
    }

    /**
     * Find All Customers
     *
     * @param int|null $limit
     * @param null $offset
     *
     * @return iCustomerEntity[]|\Traversable
     */
    function findAll(?int $limit = null, $offset = null)
    {
        $query = "SELECT * FROM {$this->table}";
        $binds = [];
        if (null !== $limit) {
            $query .= " LIMIT {$limit}";
            $binds[] = $limit;
        }
        if (null !== $offset) {
            $offset = (int) $offset;
            $query .= ", $offset";
            $binds[] = $offset;
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($binds);
        $customers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return new GeneratorWrapper($customers, function ($value, $_) {
            return (new CustomerEntity())
                ->setUid($value['customer_id'])
                ->setFullname($value['fullname'])
                ->setDateCreated(new \DateTime($value['created_date']));
        });
    }

    /**
     * Find All Has Name Like Given Name
     *
     * @param string $name
     * @param int|null $limit
     * @param null $offset
     *
     * @return iCustomerEntity[]|\Traversable
     */
    function findAllHasName($name, ?int $limit = null, $offset = null)
    {
        $query = "SELECT * FROM {$this->table} WHERE fullname like '%{$name}%'";
        $binds = [];
        if (null !== $limit) {
            $query .= "LIMIT {$limit}";
            $binds[] = $limit;
        }
        if (null !== $offset) {
            $offset = (int) $offset;
            $query .= ", $offset";
            $binds[] = $offset;
        }

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($binds);
        $customers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return new GeneratorWrapper($customers, function ($value, $_) {
            return (new CustomerEntity())
                ->setUid($value['customer_id'])
                ->setFullname($value['fullname'])
                ->setDateCreated(new \DateTime($value['created_date']));
        });
    }
}
