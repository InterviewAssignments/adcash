<?php
namespace Module\AdcashAsm\Repositories\Driver\Mysql;

use Module\AdcashAsm\Interfaces\Repositories\iRepoUsers;
use Module\AdcashAsm\Repositories\Entities\UserEntity;


class UsersRepo
    implements iRepoUsers
{
    /** @var \PDO */
    protected $pdo;
    protected $table = 'users';


    /**
     * UserRepo
     *
     * @param \PDO $pdoDriver
     */
    function __construct(\PDO $pdoDriver)
    {
        $this->pdo = $pdoDriver;
    }


    /**
     * Find User By Combination Of Username/Password (identifier/credential)
     *
     * @param string $username
     * @param string $credential
     *
     * @return mixed|false
     */
    function findOneByUserPass($username, $credential)
    {
        $stmt = $this->pdo->prepare(
            "SELECT * FROM {$this->table} WHERE username=:username AND password=:password"
        );
        $stmt->execute(['username' => $username, 'password' => md5($credential)]);
        if (! $user = $stmt->fetch(\PDO::FETCH_ASSOC) )
            return false;

        return (new UserEntity)
            ->setUid($user['user_id'])
            ->setUsername($user['username'])
            ->setPassword($user['password'])
            ->setFullname($user['fullname']);
    }
}
