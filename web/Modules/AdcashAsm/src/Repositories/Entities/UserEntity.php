<?php
namespace Module\AdcashAsm\Repositories\Entities;

use Module\AdcashAsm\Interfaces\Entities\iUserEntity;
use Poirot\Std\Struct\aDataOptions;


class UserEntity
    extends aDataOptions
    implements iUserEntity
{
    protected $uid;
    protected $username;
    protected $password;
    protected $fullname;


    /**
     * Set User Unique System Identifier
     *
     * @param mixed $uid
     *
     * @return $this
     */
    function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * User Unique System Identifier
     *
     * @return mixed
     */
    function getUid()
    {
        return $this->uid;
    }

    /**
     * Set Username
     *
     * @param string $username
     *
     * @return $this
     */
    function setUsername(string $username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Username
     *
     * @return string
     */
    function getUsername()
    {
        return $this->username;
    }

    /**
     * Set Password
     *
     * @param string $password
     *
     * @return $this
     */
    function setPassword(string $password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Password in hashed format
     *
     * @return string
     */
    function getPassword()
    {
        return $this->password;
    }

    /**
     * Set Fullname
     *
     * @param string|null $fullname
     *
     * @return $this
     */
    function setFullname(?string $fullname)
    {
        $this->fullname = $fullname;
        return $this;
    }

    /**
     * Get User Fullname
     *
     * @return string
     */
    function getFullname()
    {
        return $this->fullname;
    }
}
