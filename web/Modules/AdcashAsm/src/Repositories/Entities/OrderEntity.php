<?php
namespace Module\AdcashAsm\Repositories\Entities;

use Module\AdcashAsm\Interfaces\Entities\iCustomerEntity;
use Module\AdcashAsm\Interfaces\Entities\iOrderEntity;
use Poirot\Std\Struct\aDataOptions;
use Poirot\ValueObjects\Amount\AmountObject;


class OrderEntity
    extends aDataOptions
    implements iOrderEntity
{
    /** @var mixed */
    protected $uid;
    /** @var iCustomerEntity */
    protected $customer;
    /** @var AmountObject */
    protected $totalPrice;
    /** @var AmountObject */
    protected $totalPromoPrice;
    protected $countItems = 0;
    protected $countTotalQuantities = 0;
    protected $status = iOrderEntity::STATUS_UNPAID;
    /** @var \DateTime */
    protected $dateCreated;


    /**
     * Set User Unique System Identifier
     *
     * @param mixed $uid
     *
     * @return $this
     */
    function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * User Unique System Identifier
     *
     * @return mixed
     */
    function getUid()
    {
        return $this->uid;
    }

    /**
     * Set Customer
     *
     * @param iCustomerEntity $customer
     *
     * @return $this
     */
    function setCustomer(iCustomerEntity $customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * Customer
     *
     * @return iCustomerEntity
     */
    function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Total Price
     *
     * @param AmountObject $price
     *
     * @return $this
     */
    function setTotalPrice(AmountObject $price)
    {
        $this->totalPrice = $price;
        return $this;
    }

    /**
     * Total Order Price
     *
     * @return AmountObject
     */
    function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Total Order Promotion Price
     *
     * @param AmountObject $price
     *
     * @return OrderEntity
     */
    function setTotalPromoPrice(AmountObject $price)
    {
        $this->totalPromoPrice = $price;
        return $this;
    }

    /**
     * Total Order Promotion Price
     *
     * @return AmountObject
     */
    function getTotalPromoPrice()
    {
        return $this->totalPromoPrice;
    }

    /**
     * Count Items
     *
     * @param int $count
     *
     * @return $this
     */
    function setCountItems(int $count)
    {
        $this->countItems = $count;
        return $this;
    }

    /**
     * Get Count Items
     *
     * @return int
     */
    function getCountItems()
    {
        return $this->countItems;
    }

    /**
     * Count Total Quantities
     *
     * @param int $count
     *
     * @return $this
     */
    function setCountTotalQuantities(int $count)
    {
        $this->countTotalQuantities = $count;
        return $this;
    }

    /**
     * Get Count Total Quantities
     *
     * @return int
     */
    function getCountTotalQuantities()
    {
        return $this->countTotalQuantities;
    }

    /**
     * Set Status
     *
     * @param string $status
     *
     * @return $this
     */
    function setStatus(string $status)
    {
        // TODO validation, using NSplEnum::class
        $this->status = $status;
        return $this;
    }

    /**
     * Order Status
     *
     * @return string
     */
    function getStatus()
    {
        return $this->status;
    }

    /**
     * Set Date Time Created
     *
     * @param \DateTime $datetime
     *
     * @return $this
     */
    function setDateCreated(\DateTime $datetime)
    {
        $this->dateCreated = $datetime;
        return $this;
    }

    /**
     * Order Created Date
     *
     * @return \DateTime
     */
    function getDateCreated()
    {
        if (! $this->dateCreated )
            $this->dateCreated = new \DateTime;

        return $this->dateCreated;
    }
}
