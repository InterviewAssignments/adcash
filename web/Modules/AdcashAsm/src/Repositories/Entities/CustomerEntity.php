<?php
namespace Module\AdcashAsm\Repositories\Entities;

use Module\AdcashAsm\Interfaces\Entities\iCustomerEntity;
use Poirot\Std\Struct\aDataOptions;


class CustomerEntity
    extends aDataOptions
    implements iCustomerEntity
{
    /** @var mixed */
    protected $uid;
    /** @var string */
    protected $fullname;
    /** @var \DateTime */
    protected $dateCreated;


    /**
     * Set User Unique System Identifier
     *
     * @param mixed $uid
     *
     * @return $this
     */
    function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * User Unique System Identifier
     *
     * @return mixed
     */
    function getUid()
    {
        return $this->uid;
    }

    /**
     * Set Customer Fullname
     *
     * @param string $fullname
     *
     * @return $this
     */
    function setFullname(string $fullname)
    {
        $this->fullname = $fullname;
        return $this;
    }

    /**
     * Get Customer Fullname
     *
     * @return string
     */
    function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set Date Time Created
     *
     * @param \DateTime $datetime
     *
     * @return $this
     */
    function setDateCreated(\DateTime $datetime)
    {
        $this->dateCreated = $datetime;
        return $this;
    }

    /**
     * Entity Created Date
     *
     * @return \DateTime
     */
    function getDateCreated()
    {
        if (! $this->dateCreated )
            $this->dateCreated = new \DateTime;

        return $this->dateCreated;
    }
}
