<?php
namespace Module\AdcashAsm\Repositories\Entities;

use Module\AdcashAsm\Interfaces\Entities\iProductEntity;
use Poirot\Std\Struct\DataOptionsOpen;
use Poirot\ValueObjects\Amount\AmountObject;


class ProductEntity
    extends DataOptionsOpen
    implements iProductEntity
{
    /** @var mixed */
    protected $uid;
    /** @var string */
    protected $title;
    /** @var string */
    protected $description;
    /** @var string */
    protected $thumbUrl;
    /** @var AmountObject */
    protected $price;
    /** @var \DateTime */
    protected $dateCreated;
    /** @var \DateTime */
    protected $dateUpdated;
    /** @var bool */
    protected $isAvailable = true;


    /**
     * Set User Unique System Identifier
     *
     * @param mixed $uid
     *
     * @return $this
     */
    function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * User Unique System Identifier
     *
     * @return mixed
     */
    function getUid()
    {
        return $this->uid;
    }

    /**
     * Title
     *
     * @param string $title
     *
     * @return $this
     */
    function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Title
     *
     * @return string
     */
    function getTitle()
    {
        return $this->title;
    }

    /**
     * Description
     *
     * @param string|null $description
     *
     * @return $this
     */
    function setDescription(?string $description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Description
     *
     * @return string
     */
    function getDescription()
    {
        return $this->description;
    }

    /**
     * Set Thumbnail Image Url
     *
     * @param string $url
     *
     * @return $this
     */
    function setThumbUrl(?string $url)
    {
        $this->thumbUrl = $url;
        return $this;
    }

    /**
     * Get Thumbnail Image Url
     *
     * @return string
     */
    function getThumbUrl()
    {
        return $this->thumbUrl;
    }

    /**
     * Price
     *
     * @param AmountObject $amount
     *
     * @return $this
     */
    function setPrice(AmountObject $amount)
    {
        $this->price = $amount;
        return $this;
    }

    /**
     * Price
     *
     * @return AmountObject
     */
    function getPrice()
    {
        return $this->price;
    }

    /**
     * Set Date Time Created
     *
     * @param \DateTime $datetime
     *
     * @return $this
     */
    function setDateCreated(\DateTime $datetime)
    {
        $this->dateCreated = $datetime;
        return $this;
    }

    /**
     * Entity Created Date
     *
     * @return \DateTime
     */
    function getDateCreated()
    {
        if (! $this->dateCreated )
            $this->dateCreated = new \DateTime;

        return $this->dateCreated;
    }

    /**
     * Set Date Time Updated
     *
     * @param \DateTime $datetime
     *
     * @return $this
     */
    function setDateUpdated(?\DateTime $datetime)
    {
        $this->dateUpdated = $datetime;
        return $this;
    }

    /**
     * Entity Updated Date
     *
     * @return \DateTime
     */
    function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * Set Available
     *
     * @param bool $flag
     *
     * @return $this
     */
    function setAvailable(bool $flag = true)
    {
        $this->isAvailable = $flag;
        return $this;
    }

    /**
     * Is Available?
     *
     * @return bool
     */
    function isAvailable()
    {
        return $this->isAvailable;
    }
}
