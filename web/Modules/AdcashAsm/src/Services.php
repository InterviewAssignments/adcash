<?php
/*
 * Facade Class To Easy Access To Services
 *
 */
namespace Module\AdcashAsm
{
    /**
     * @method static \PDO PdoClient()
     */
    class Services extends \IOC
    {
        const PdoClient = 'PdoClient';
    }
}
