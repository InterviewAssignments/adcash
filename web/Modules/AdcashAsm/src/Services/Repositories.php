<?php
/*
 * Facade Class To Easy Access To Services
 *
 */
namespace Module\AdcashAsm\Services
{
    use Module\AdcashAsm\Interfaces\Repositories\iRepoCustomers;
    use Module\AdcashAsm\Interfaces\Repositories\iRepoOrders;
    use Module\AdcashAsm\Interfaces\Repositories\iRepoProducts;
    use Module\AdcashAsm\Interfaces\Repositories\iRepoUsers;


    /**
     * @method static iRepoUsers UsersRepo()
     * @method static iRepoCustomers CustomersRepo()
     * @method static iRepoProducts ProductsRepo()
     * @method static iRepoOrders OrdersRepo()
     */
    class Repositories extends \IOC
    {
        const UsersRepo     = 'UsersRepo';
        const CustomersRepo = 'CustomersRepo';
        const ProductsRepo  = 'ProductsRepo';
        const OrdersRepo    = 'OrdersRepo';
    }
}
