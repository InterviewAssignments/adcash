<?php
namespace Module\AdcashAsm\Services;

use Module\AdcashAsm\Module;
use Poirot\Ioc\Container\Service\aServiceContainer;


class PdoClientService
    extends aServiceContainer
{
    static protected $conn = [
        #'client' => $conn
    ];

    /** @var string */
    protected $client = 'master';


    /**
     * @inheritdoc
     *
     * @return \PDO
     * @throws \Exception
     */
    final function newService()
    {
        $client = $this->client;
        if ( isset(static::$conn[$client]) )
            return static::$conn[$client];


        $conf = \Poirot\config(Module::class, self::class, 'clients', $client);
        if (! $conf )
            throw new \InvalidArgumentException(sprintf(
                'Client %s has no configuration settings.'
                    , $client
            ));

        $conn = new \PDO("mysql:host={$conf['server']};dbname={$conf['db']}"
            , $conf['user']
            , $conf['pass']
        );

        // set the PDO error mode to exception
        $conn->setAttribute(\PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'utf8'");
        $conn->setAttribute(\PDO::MYSQL_ATTR_INIT_COMMAND, "SET CHARACTER SET 'utf8'");
        $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $conn->exec('SET NAMES \'utf8\'');
        $conn->exec('SET CHARACTER SET \'utf8\'');

        return static::$conn[$client] = $conn;
    }

    // Options:

    /**
     * Set Client Name
     *
     * @param string $client
     *
     * @return PdoClientService
     */
    function setClientName(string $client)
    {
        $this->client = $client;
        return $this;
    }
}
