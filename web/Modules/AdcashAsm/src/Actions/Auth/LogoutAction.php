<?php
namespace Module\AdcashAsm\Actions\Auth;

use Module\AdcashAsm\Actions\AbstractAdcashAction;
use Module\HttpFoundation\Events\Listener\ListenerDispatchResult;
use Module\HttpFoundation\Response\ResponseRedirect;


class LogoutAction
    extends AbstractAdcashAction
{
    function __invoke()
    {
        if ($this->getAuthenticator()->hasAuthenticated())
            $this->getAuthenticator()->identifier()->signOut();


        $redirectUri = \Module\HttpFoundation\Actions::url('main/home');
        return [ ListenerDispatchResult::RESULT_DISPATCH => new ResponseRedirect( $redirectUri ) ];
    }
}
