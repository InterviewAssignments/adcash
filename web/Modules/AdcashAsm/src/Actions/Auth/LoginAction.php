<?php
namespace Module\AdcashAsm\Actions\Auth;

use Module\AdcashAsm\Actions\AbstractAdcashAction;
use Module\AdcashAsm\Forms\SigninHydrate;
use Module\HttpFoundation\Actions\Url;
use Module\HttpFoundation\Events\Listener\ListenerDispatchResult;
use Module\HttpFoundation\Response\ResponseRedirect;
use Poirot\AuthSystem\Authenticate\Exceptions\AuthenticationError;
use Poirot\Http\HttpMessage\Request\Plugin;
use Poirot\Http\Interfaces\iHttpRequest;
use Poirot\Std\Exceptions\UnexpectedInputValueError;


class LoginAction
    extends AbstractAdcashAction
{
    const FLASH_MESSAGE_ID = __CLASS__;

    /** @var iHttpRequest */
    protected $request;


    /**
     * LoginAction
     *
     * @param iHttpRequest $httpRequest @IoC /HttpRequest
     */
    function __construct(iHttpRequest $httpRequest)
    {
        $this->request = $httpRequest;
    }


    function __invoke()
    {
        \Module\HttpFoundation\Actions::htmlHeadTitle()->appendTitle('Signin or register to continue');


        ## Check if user currently is authenticated continue flow by redirect
        #
        if ($identifier = $this->getAuthenticator()->hasAuthenticated())
            // User Is Logged In; Continue Redirection
            return $this->_handleAlreadyAuthenticated();

        if (! Plugin\MethodType::_($this->request)->isPost() )
            // display login page
            return null;


        // Login Credential Sent ...

        # Create Signin Form From Http Request
        #
        $hydrateReqData = new SigninHydrate($this->request);

        try {
            $hydrateReqData->assertValidate();
        } catch (UnexpectedInputValueError $e) {
            return $this->_handleUnexpectedInputValues($e);
        }

        ## Authenticate User
        #
        try {
            return $this->_handleLoginWithPassword($hydrateReqData);

        } catch (AuthenticationError $e) {
            $this->_flashMessage()->error('Username or Password Is Not Correct.');

        } catch (UnexpectedInputValueError $e) {
            $this->_flashMessage()->error($e->getMessage());

        } catch (\Exception $e) {
            $this->_flashMessage()->error('Service Not Available At The Moment. Try Again Later.');

        } finally {
            // Critical System Exceptions Should Be logged Down, Log Error.
            if (isset($e))
                // TODO log error
                VOID;
        }

        // refresh page
        return $this->_responseRedirect();
    }


    //..

    /**
     * @param SigninHydrate $hydratePost
     * @return array
     * @throws \Exception
     */
    protected function _handleLoginWithPassword(SigninHydrate $hydratePost)
    {
        $identifier = $this->getAuthenticator()
            ->authenticate([
                'username' => $hydratePost->getIdentifier(),
                'password' => $hydratePost->getCredential(),
            ]);

        // Signin Identifier (save session); login user
        $identifier->signIn();

        // refresh
        return $this->_responseRedirect();
    }

    protected function _handleAlreadyAuthenticated()
    {
        $queryParams = Plugin\ParseRequestData::_($this->request)->parseQueryParams();
        $continue    = $queryParams['continue']
            ?? (string) \Module\HttpFoundation\Actions::url('main/panel/dashboard');

        return $this->_responseRedirect($continue);
    }

    protected function _handleUnexpectedInputValues(UnexpectedInputValueError $e)
    {
        do {
            if ($e->getParameterName() == null)
                break;

            switch ( $e->getParameterName() ) {
                case 'identifier':
                    switch ($e->getError()) {
                        case UnexpectedInputValueError::ERR_REQUIRED:
                            $this->_flashMessage()->error('Couldn`t Login Without Knowing Who You Are; Enter Your Username.');
                            break;
                    }
                    break;

                case 'credential':
                    switch ($e->getError()) {
                        case UnexpectedInputValueError::ERR_REQUIRED:
                            $this->_flashMessage()->error('Credential Is Required To Login.');
                            break;
                        case UnexpectedInputValueError::ERR_MIN_LENGTH:
                            $this->_flashMessage()->error('Password Less Than 5 Character Not Allowed.');
                            break;
                    }
                    break;

                default:
                    $this->_flashMessage()->error('Unknown Error While Validating Input Data.');;
            }
        } while ($e = $e->getPrevious());

        // refresh; redirect to current page to display flash messages; cleanup post data
        return $this->_responseRedirect();
    }

    /**
     * Flash Message
     *
     * @return \Module\HttpFoundation\Actions\FlashMessage
     */
    private function _flashMessage()
    {
        return \Module\HttpFoundation\Actions::flashMessage(self::FLASH_MESSAGE_ID);
    }
}
