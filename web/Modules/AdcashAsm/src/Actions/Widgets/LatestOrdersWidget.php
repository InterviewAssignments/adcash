<?php
namespace Module\AdcashAsm\Actions\Widgets;

use Module\AdcashAsm\Interfaces\Repositories\iRepoOrders;


class LatestOrdersWidget
{
    /** @var iRepoOrders */
    protected $repoOrders;


    /**
     * LatestOrdersWidget
     *
     * @param iRepoOrders $repoOrders @IoC /module/AdcashAsm/services/repositories/OrdersRepo
     */
    function __construct(iRepoOrders $repoOrders)
    {
        $this->repoOrders = $repoOrders;
    }


    /**
     * @return string
     */
    function __invoke()
    {
        $orders = $this->repoOrders->findAll(10, null, iRepoOrders::ORDER_DESC);


        // Render Widget
        return (string) \Module\Foundation\Actions::view(
            'widgets/orders/latest_orders'
            , [ 'orders' => $orders ]
        );
    }
}
