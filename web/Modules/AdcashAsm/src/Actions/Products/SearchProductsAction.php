<?php
namespace Module\AdcashAsm\Actions\Products;

use Module\AdcashAsm\Actions\AbstractAdcashAction;
use Module\AdcashAsm\Interfaces\Entities\iProductEntity;
use Module\AdcashAsm\Interfaces\Repositories\iRepoProducts;
use Module\HttpFoundation\Events\Listener\ListenerDispatchResult;
use Poirot\Http\HttpMessage\Request\Plugin\ParseRequestData;
use Poirot\Http\Interfaces\iHttpRequest;


class SearchProductsAction
    extends AbstractAdcashAction
{
    /** @var iHttpRequest */
    protected $request;
    /** @var iRepoProducts */
    protected $repoProducts;


    /**
     * SearchProductsAction
     *
     * @param iHttpRequest  $httpRequest  @IoC /HttpRequest
     * @param iRepoProducts $repoProducts @IoC /module/AdcashAsm/services/repositories/ProductsRepo
     */
    function __construct(iHttpRequest $httpRequest, iRepoProducts $repoProducts)
    {
        $this->request = $httpRequest;
        $this->repoProducts = $repoProducts;
    }


    function __invoke()
    {
        $result = [];
        $reqData = ParseRequestData::_($this->request)->parseQueryParams();
        if (isset($reqData['q'])) {
            $customers = $this->repoProducts->findAllAvailableWithName($reqData['q']);
        } else {
            $customers = $this->repoProducts->findAll();
        }

        /** @var iProductEntity $csm */
        foreach ($customers as $csm) {
            $result[] = [
                'id'   => $csm->getUid(),
                'text' => $csm->getTitle(),
                'img'  => $csm->getThumbUrl(),
            ];
        }

        return [
            ListenerDispatchResult::RESULT_DISPATCH => [
                'results' => $result,
            ]
        ];
    }
}
