<?php
namespace Module\AdcashAsm\Actions;

use Module\AdcashAsm\Module;
use Module\Foundation\Actions\aAction;
use Module\HttpFoundation\Actions\Url;
use Module\HttpFoundation\Response\ResponseRedirect;
use Poirot\AuthSystem\Authenticate\Authenticator;
use Poirot\AuthSystem\Authenticate\Interfaces\iAuthenticator;


abstract class AbstractAdcashAction
    extends aAction
{
    // Helpers:

    /**
     * Authenticator
     *
     * @return iAuthenticator|Authenticator
     */
    function getAuthenticator()
    {
        return \Module\Authorization\Actions::Authenticator(Module::AUTH_REALM);
    }

    /**
     * Response Redirect
     *
     * @param null|string $url
     *
     * @return array
     */
    protected function _responseRedirect($url = null)
    {
        if ($url === null)
            // redirect to itself
            $url = \Module\HttpFoundation\Actions::url(null, [], Url::DEFAULT_INSTRUCT|Url::APPEND_CURRENT_REQUEST_QUERY);

        return $this->respondResult(new ResponseRedirect($url));
    }
}
