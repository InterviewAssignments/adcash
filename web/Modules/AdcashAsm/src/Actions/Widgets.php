<?php
namespace Module\AdcashAsm\Actions
{
    use Module\AdcashAsm\Actions\Widgets\LatestOrdersWidget;


    /**
     * @see LatestOrdersWidget
     * @method static string LatestOrders()
     * ....................................
     *
     */
    class Widgets extends \IOC
    {
        const LatestOrders = 'LatestOrders';
    }
}
