<?php
namespace Module\AdcashAsm\Actions\Orders;

use Module\AdcashAsm\Repositories;
use Module\AdcashAsm\Actions\AbstractAdcashAction;
use Module\AdcashAsm\Interfaces\Repositories\iRepoOrders;
use Module\AdcashAsm\Interfaces\Repositories\iRepoProducts;
use Poirot\Http\HttpMessage\Request\Plugin\MethodType;
use Poirot\Http\HttpResponse;
use Poirot\Http\Interfaces\iHttpRequest;


class DeleteOrderAction
    extends AbstractAdcashAction
{
    const FLASH_MESSAGE_ID = __CLASS__;

    /** @var iHttpRequest */
    protected $request;
    /** @var iRepoOrders */
    protected $repoOrders;
    /** @var iRepoProducts */
    protected $repoProducts;


    /**
     * NewOrderAction
     *
     * @param iHttpRequest  $httpRequest  @IoC /HttpRequest
     * @param iRepoOrders   $repoOrders   @IoC /module/AdcashAsm/services/repositories/OrdersRepo
     * @param iRepoProducts $repoProducts @IoC /module/AdcashAsm/services/repositories/ProductsRepo
     */
    function __construct(
        iHttpRequest $httpRequest
        , iRepoOrders $repoOrders
        , iRepoProducts $repoProducts
    ) {
        $this->request      = $httpRequest;
        $this->repoOrders   = $repoOrders;
        $this->repoProducts = $repoProducts;
    }


    function __invoke($orderId = null)
    {
        // TODO we should'nt allow delete an item by just calling this
        //      url address if plane http called display a form with accept button
        //      and delete item on POST when accepted by user

        $this->repoOrders->deleteOneByUid($orderId);

        if (MethodType::_($this->request)->isXmlHttpRequest()) {
            return $this->respondResult(
                (new HttpResponse)->setStatusCode(204)
            );
        }

        $this->_flashMessage()->info( 'Order Deleted Successfully.' );
        return $this->_responseRedirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Flash Message
     *
     * @return \Module\HttpFoundation\Actions\FlashMessage
     */
    private function _flashMessage()
    {
        return \Module\HttpFoundation\Actions::flashMessage(self::FLASH_MESSAGE_ID);
    }
}
