<?php
namespace Module\AdcashAsm\Actions\Orders;

use Module\AdcashAsm\Repositories;
use Module\AdcashAsm\Actions\AbstractAdcashAction;
use Module\AdcashAsm\Forms\OrderHydrate;
use Module\AdcashAsm\Interfaces\Repositories\iRepoOrders;
use Module\AdcashAsm\Interfaces\Repositories\iRepoProducts;
use Poirot\Application\Exception\ErrorRouteNotMatch;
use Poirot\Http\HttpMessage\Request\Plugin\MethodType;
use Poirot\Http\Interfaces\iHttpRequest;
use Poirot\Std\Exceptions\UnexpectedInputValueError;


class EditOrderAction
    extends AbstractAdcashAction
{
    const FLASH_MESSAGE_ID = __CLASS__;

    /** @var iHttpRequest */
    protected $request;
    /** @var iRepoOrders */
    protected $repoOrders;
    /** @var iRepoProducts */
    protected $repoProducts;


    /**
     * NewOrderAction
     *
     * @param iHttpRequest  $httpRequest  @IoC /HttpRequest
     * @param iRepoOrders   $repoOrders   @IoC /module/AdcashAsm/services/repositories/OrdersRepo
     * @param iRepoProducts $repoProducts @IoC /module/AdcashAsm/services/repositories/ProductsRepo
     */
    function __construct(
        iHttpRequest $httpRequest
        , iRepoOrders $repoOrders
        , iRepoProducts $repoProducts
    ) {
        $this->request      = $httpRequest;
        $this->repoOrders   = $repoOrders;
        $this->repoProducts = $repoProducts;
    }


    function __invoke($orderId = null)
    {
        if (! $orderEntity = $this->repoOrders->findOneByUid($orderId) )
            throw new ErrorRouteNotMatch();


        if ( MethodType::_($this->request)->isPost() )
            return $this->_handleSaveOrder($orderEntity);


        $products = $this->repoOrders->findAllProductsBelongToOrder($orderEntity->getUid());

        return $this->respondResult([
            'order'    => $orderEntity,
            'products' => $products,
        ]);
    }

    //..

    protected function _handleSaveOrder($orderEntity)
    {
        // Save New Order

        # Create New Order Form From Http Request
        #
        $hydrateReqData = new OrderHydrate($this->request);

        try {
            $hydrateReqData->assertValidate();

            // facade access to repos.
            $customer = \Module\AdcashAsm\Services\Repositories::CustomersRepo()
                ->findOneByUID($hydrateReqData->getCustomer());

            if (! $customer )
                throw new UnexpectedInputValueError('Customer Id is Not Valid.');

            $productSaving = [];
            foreach ($hydrateReqData->getProducts() as $i => $productId)
            {
                if (! $productEntity = $this->repoProducts->findOneByUID($productId) )
                    throw new UnexpectedInputValueError(sprintf(
                        'Product with id (%s) not found.', $productId));

                if (! $productEntity->isAvailable() )
                    throw new UnexpectedInputValueError(sprintf(
                        'Product with id (%s) not available.', $productId));

                $productSaving[] = [
                    'product' => $productEntity,
                    'count'   => $hydrateReqData->getProductQuantities()[$i],
                ];
            }

        } catch (UnexpectedInputValueError $e) {
            return $this->_handleUnexpectedInputValues($e);
        }


        ## Insert New Order And Products
        #
        try {
            $orderEntity->setCustomer($customer);
            $this->repoOrders->updateOrderAndProducts($orderEntity, $productSaving);

        } catch (\Exception $e) {
            $this->_flashMessage()->error($e->getMessage());
            return $this->_responseRedirect();
        }


        $this->_flashMessage()->info( 'Order Saved Successfully' );
        return $this->_responseRedirect();
    }

    protected function _handleUnexpectedInputValues(UnexpectedInputValueError $e)
    {
        do {
            $this->_flashMessage()->error( $e->getMessage() );
        } while ($e = $e->getPrevious());

        // refresh; redirect to current page to display flash messages; cleanup post data
        return $this->_responseRedirect();
    }

    /**
     * Flash Message
     *
     * @return \Module\HttpFoundation\Actions\FlashMessage
     */
    private function _flashMessage()
    {
        return \Module\HttpFoundation\Actions::flashMessage(self::FLASH_MESSAGE_ID);
    }
}
