<?php
namespace Module\AdcashAsm\Actions\Orders;

use Module\AdcashAsm\Repositories;
use Module\AdcashAsm\Actions\AbstractAdcashAction;
use Module\AdcashAsm\Interfaces\Repositories\iRepoOrders;
use Module\AdcashAsm\Interfaces\Repositories\iRepoProducts;
use Poirot\Http\HttpMessage\Request\Plugin\ParseRequestData;
use Poirot\Http\Interfaces\iHttpRequest;


class AllOrdersAction
    extends AbstractAdcashAction
{
    /** @var iHttpRequest */
    protected $request;
    /** @var iRepoOrders */
    protected $repoOrders;
    /** @var iRepoProducts */
    protected $repoProducts;


    /**
     * NewOrderAction
     *
     * @param iHttpRequest  $httpRequest  @IoC /HttpRequest
     * @param iRepoOrders   $repoOrders   @IoC /module/AdcashAsm/services/repositories/OrdersRepo
     * @param iRepoProducts $repoProducts @IoC /module/AdcashAsm/services/repositories/ProductsRepo
     */
    function __construct(
        iHttpRequest $httpRequest
        , iRepoOrders $repoOrders
        , iRepoProducts $repoProducts
    ) {
        $this->request      = $httpRequest;
        $this->repoOrders   = $repoOrders;
        $this->repoProducts = $repoProducts;
    }


    function __invoke()
    {
        $filters = $this->_parseFiltersFromRequest();

        $date = null;
        if ($filters['from'])
            $date = new \DateTime($filters['from']);

        if ($filters['scope'] == 'customer') {
            $orders = $this->repoOrders->findAllOwnedByCustomer($filters['ident'], $date, 10, null, iRepoOrders::ORDER_DESC);

        } elseif ($filters['scope'] == 'product') {
            $orders = $this->repoOrders->findAllThatHasProduct($filters['ident'], $date, 10, null, iRepoOrders::ORDER_DESC);

        } else {
            $orders = $this->repoOrders->findAllAfterDate($date, 10, null, iRepoOrders::ORDER_DESC);
        }


        // Make Response
        return $this->respondResult([
            'orders'  => $orders,
            'filters' => $filters,
        ]);
    }

    // ..

    protected function _parseFiltersFromRequest()
    {
        $reqData = ParseRequestData::_($this->request)->parseQueryParams();

        $from = false;
        if (isset($reqData['from']) && $from = strtotime($reqData['from']) ?? false )
            $from = date('Y-m-d', $from);

        return [
            'from'  => $from,
            'scope' => $reqData['scope'] ?? false,
            'ident' => $reqData['ident'] ?? false,
        ];
    }
}
