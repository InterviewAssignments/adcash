<?php
namespace Module\AdcashAsm\Actions\Customers;

use Module\AdcashAsm\Actions\AbstractAdcashAction;
use Module\AdcashAsm\Interfaces\Entities\iCustomerEntity;
use Module\AdcashAsm\Interfaces\Repositories\iRepoCustomers;
use Module\HttpFoundation\Events\Listener\ListenerDispatchResult;
use Poirot\Http\HttpMessage\Request\Plugin\ParseRequestData;
use Poirot\Http\Interfaces\iHttpRequest;


class SearchCustomersAction
    extends AbstractAdcashAction
{
    /** @var iHttpRequest */
    protected $request;
    /** @var iRepoCustomers */
    protected $repoCustomers;


    /**
     * SearchCustomersAction
     *
     * @param iHttpRequest   $httpRequest   @IoC /HttpRequest
     * @param iRepoCustomers $repoCustomers @IoC /module/AdcashAsm/services/repositories/CustomersRepo
     */
    function __construct(iHttpRequest $httpRequest, iRepoCustomers $repoCustomers)
    {
        $this->request = $httpRequest;
        $this->repoCustomers = $repoCustomers;
    }


    function __invoke()
    {
        /**
         * Note: here we try to prepare specific data feed that will use by select2,
         * one of good practices is to avoid dependent on technology that we use on frontend
         * and just return domain specific application standard response. and use some sort of
         * Data Transform Objects to achieve the data structure that we need.
         *
         * it could be achieved with many solutions but one of them is to use built-in transformers
         * for instance take a look at this @see CartRenderer that could be attached to renderer
         * an example of configuration on this file: cor-http_renderer.conf.php
         */

        $result = [];

        // TODO I ignored implementation of pagination here by now,
        //      to demonstrate this functionality we have orders page.

        $reqData = ParseRequestData::_($this->request)->parseQueryParams();
        if (isset($reqData['q'])) {
            $customers = $this->repoCustomers->findAllHasName($reqData['q']);
        } else {
            $customers = $this->repoCustomers->findAll();
        }

        /** @var iCustomerEntity $csm */
        foreach ($customers as $csm) {
            $result[] = [
                'id'   => $csm->getUid(),
                'text' => $csm->getFullname(),
            ];
        }

        return [
            ListenerDispatchResult::RESULT_DISPATCH => [
                'results' => $result,
            ]
        ];
    }
}
