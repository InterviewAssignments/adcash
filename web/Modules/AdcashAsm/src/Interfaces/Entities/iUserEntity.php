<?php
namespace Module\AdcashAsm\Interfaces\Entities;


interface iUserEntity
{
    /**
     * User Unique System Identifier
     *
     * @return mixed
     */
    function getUid();

    /**
     * Username
     *
     * @return string
     */
    function getUsername();

    /**
     * Password in hashed format
     *
     * @return string
     */
    function getPassword();

    /**
     * Get User Fullname
     *
     * @return string
     */
    function getFullname();
}
