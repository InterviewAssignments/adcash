<?php
namespace Module\AdcashAsm\Interfaces\Entities;


interface iCustomerEntity
{
    /**
     * User Unique System Identifier
     *
     * @return mixed
     */
    function getUid();

    /**
     * Get User Fullname
     *
     * @return string
     */
    function getFullname();

    /**
     * Entity Created Date
     *
     * @return \DateTime
     */
    function getDateCreated();
}
