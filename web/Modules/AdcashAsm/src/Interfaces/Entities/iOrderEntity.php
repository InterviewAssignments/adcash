<?php
namespace Module\AdcashAsm\Interfaces\Entities;

use Poirot\ValueObjects\Amount\AmountObject;


interface iOrderEntity
{
    const STATUS_PAID   = 'paid';
    const STATUS_UNPAID = 'unpaid';


    /**
     * User Unique System Identifier
     *
     * @return mixed
     */
    function getUid();

    /**
     * Customer
     *
     * @return iCustomerEntity
     */
    function getCustomer();

    /**
     * Total Order Price
     *
     * @return AmountObject
     */
    function getTotalPrice();

    /**
     * Total Order Promotion Price
     *
     * @return AmountObject
     */
    function getTotalPromoPrice();

    /**
     * Get Count Items
     *
     * @return int
     */
    function getCountItems();

    /**
     * Get Count Total Quantities
     *
     * @return int
     */
    function getCountTotalQuantities();

    /**
     * Order Status
     *
     * @return string
     */
    function getStatus();

    /**
     * Order Created Date
     *
     * @return \DateTime
     */
    function getDateCreated();
}
