<?php
namespace Module\AdcashAsm\Interfaces\Entities;

use Poirot\ValueObjects\Amount\AmountObject;


interface iProductEntity
{
    /**
     * User Unique System Identifier
     *
     * @return mixed
     */
    function getUid();

    /**
     * Title
     *
     * @return string
     */
    function getTitle();

    /**
     * Description
     *
     * @return string
     */
    function getDescription();

    /**
     * Get Thumbnail Image Url
     *
     * @return string
     */
    function getThumbUrl();

    /**
     * Price
     *
     * @return AmountObject
     */
    function getPrice();

    /**
     * Entity Created Date
     *
     * @return \DateTime
     */
    function getDateCreated();

    /**
     * Entity Updated Date
     *
     * @return \DateTime
     */
    function getDateUpdated();

    /**
     * Is Product Available?
     *
     * @return bool
     */
    function isAvailable();
}
