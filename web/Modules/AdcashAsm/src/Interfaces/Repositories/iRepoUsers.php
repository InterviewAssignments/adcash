<?php
namespace Module\AdcashAsm\Interfaces\Repositories;

use Module\AdcashAsm\Interfaces\Entities\iUserEntity;


interface iRepoUsers
{
    /**
     * Find User By Combination Of Username/Password (identifier/credential)
     *
     * !! This Method Is Mandatory to Implement "password" Grant
     *
     * @param string $username
     * @param string $credential
     *
     * @return iUserEntity|false
     */
    function findOneByUserPass($username, $credential);
}
