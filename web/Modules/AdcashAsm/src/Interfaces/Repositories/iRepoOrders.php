<?php
namespace Module\AdcashAsm\Interfaces\Repositories;

use Module\AdcashAsm\Interfaces\Entities\iCustomerEntity;
use Module\AdcashAsm\Interfaces\Entities\iOrderEntity;
use Module\AdcashAsm\Interfaces\Entities\iProductEntity;


interface iRepoOrders
{
    const ORDER_ASC  = 1;
    const ORDER_DESC = -1;


    /**
     * Find Entity By Given UID
     *
     * @param mixed $uid
     *
     * @return iOrderEntity|null
     */
    function findOneByUID($uid);

    /**
     * Find All Orders
     *
     * @param int|null $limit
     * @param null     $offset
     * @param int      $order
     *
     * @return iOrderEntity[]
     */
    function findAll(?int $limit = null, $offset = null, $order = self::ORDER_ASC);

    /**
     * Find All Orders After Given Date
     *
     * @param \DateTime $date
     * @param int|null $limit
     * @param null $offset
     * @param int $order
     *
     * @return iOrderEntity[]
     */
    function findAllAfterDate(?\DateTime $date = null, ?int $limit = null, $offset = null, $order = self::ORDER_ASC);

    /**
     * Find All Orders Owned By Given Customer ID
     *
     * @param $customerUid
     * @param \DateTime|null $date
     * @param int|null $limit
     * @param null $offset
     * @param int $order
     *
     * @return iOrderEntity[]
     */
    function findAllOwnedByCustomer($customerUid, ?\DateTime $date = null, ?int $limit = null, $offset = null, $order = self::ORDER_ASC);

    /**
     * Find All Orders That Has Given Product Id
     *
     * @param $productId
     * @param \DateTime|null $date
     * @param int|null $limit
     * @param null $offset
     * @param int $order
     *
     * @return iOrderEntity[]
     */
    function findAllThatHasProduct($productId, ?\DateTime $date = null, ?int $limit = null, $offset = null, $order = self::ORDER_ASC);

    /**
     * New Order With Products
     *
     * @param iCustomerEntity $customer
     * @param array           $productList ['product' => iProductEntity, ('count' => 2)]
     *
     * @return iOrderEntity Persist entity with uid
     * @throws \Exception
     */
    function insertNewOrderAndProducts(iCustomerEntity $customer, array $productList);

    /**
     * Save Changes For Order With Products
     *
     * @param iOrderEntity $order
     * @param array        $productList ['product' => iProductEntity, ('count' => 2)]
     *
     * @return iOrderEntity
     * @throws \Exception
     */
    function updateOrderAndProducts(iOrderEntity $order, array $productList);

    /**
     * Find All Products Belong To Order
     *
     * @param mixed $orderUid
     *
     * @return iProductEntity[]
     */
    function findAllProductsBelongToOrder($orderUid);

    /**
     * Delete Entity By Given ID
     *
     * @param mixed $orderId
     *
     * @return bool
     */
    function deleteOneByUid($orderId);
}
