<?php
namespace Module\AdcashAsm\Interfaces\Repositories;

use Module\AdcashAsm\Interfaces\Entities\iCustomerEntity;


interface iRepoCustomers
{
    /**
     * Find Entity By Given UID
     *
     * @param mixed $uid
     *
     * @return iCustomerEntity|null
     */
    function findOneByUID($uid);

    /**
     * Find All Customers
     *
     * @param int|null $limit
     * @param null $offset
     *
     * @return iCustomerEntity[]
     */
    function findAll(?int $limit = null, $offset = null);

    /**
     * Find All Has Name Like Given Name
     *
     * @param string $name
     * @param int|null $limit
     * @param null $offset
     *
     * @return iCustomerEntity[]
     */
    function findAllHasName($name, ?int $limit = null, $offset = null);
}
