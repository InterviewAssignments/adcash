<?php
namespace Module\AdcashAsm\Interfaces\Repositories;

use Module\AdcashAsm\Interfaces\Entities\iProductEntity;


interface iRepoProducts
{
    /**
     * Find Entity By Given UID
     *
     * @param mixed $uid
     *
     * @return iProductEntity|null
     */
    function findOneByUID($uid);

    /**
     * Find All Products
     *
     * @param int|null $limit
     * @param null $offset
     *
     * @return iProductEntity[]
     */
    function findAll(?int $limit = null, $offset = null);

    /**
     * Find All Available Products
     *
     * @param int|null $limit
     * @param null $offset
     *
     * @return iProductEntity[]
     */
    function findAllAvailable(?int $limit = null, $offset = null);

    /**
     * Find All Has Name Like Given Name
     *
     * @param string $name
     * @param int|null $limit
     * @param null $offset
     *
     * @return iProductEntity[]
     */
    function findAllAvailableWithName($name, ?int $limit = null, $offset = null);
}
